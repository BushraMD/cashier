//
//  TutorialViewC.swift
//  Cashier
//
//  Created by MSEWA on 17/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class TutorialViewC: UIViewController
{
    @IBOutlet weak var collectionViewTutorial: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var viewGradient: UIView!
    
    var arrTutorial = [Dictionary<String,String>]()
    
    //MARK: View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: IBAction Methods
    @IBAction func tapCancel(_ sender: UIButton)
    {
        moveToLoginScreen()
    }
    
    @IBAction func tapNext(_ sender: UIButton)
    {
        let visibleItems: NSArray = self.collectionViewTutorial.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        if nextItem.row < arrTutorial.count {
            self.collectionViewTutorial.scrollToItem(at: nextItem, at: .left, animated: true)
        }
        pageControl.currentPage = Int(nextItem.row)
        self.adjustButtonsOnScroll(nextItem.row)
    }
    
    @IBAction func tapBack(_ sender: UIButton)
    {
        let visibleItems: NSArray = self.collectionViewTutorial.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        if nextItem.row < arrTutorial.count && nextItem.row >= 0
        {
            self.collectionViewTutorial.scrollToItem(at: nextItem, at: .right, animated: true)
        }
        pageControl.currentPage = Int(nextItem.row)
        self.adjustButtonsOnScroll(nextItem.row)
    }
    
    @IBAction func tapFinish(_ sender: UIButton)
    {
        moveToLoginScreen()
    }
    
    //MARK: Private Methods
    func setUpView()
    {
        self.collectionViewTutorial.delegate = self
        self.collectionViewTutorial.dataSource = self
        btnBack.isHidden = true
        btnFinish.isHidden = true
        
        //Setting View's Background Gradient Colour
        let colorTop =  UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.5, 0.8]
        gradientLayer.frame = self.viewGradient.bounds
        self.viewGradient.layer.addSublayer(gradientLayer)
        
        self.collectionViewTutorial.register(UINib.init(nibName: "CellTutorial", bundle: nil), forCellWithReuseIdentifier: "cellTutorial")
       self.collectionViewTutorial.layoutIfNeeded()
       
        
        arrTutorial = [["imgName" : "mb_8" ,
                        "lblHeading" : "Easy registration process",
                        "lblDescription" : "Simple and easy registration process. Click on Register and fill in the details to get registered in no time."],
                       ["imgName" : "mb_10" ,
                        "lblHeading" : "Generate your Cashier Mastercard in no time",
                        "lblDescription" : "As soon as you login, your virtual Cashier Card is generated and will be shown on the screen."],
                       ["imgName" : "mb_9" ,
                        "lblHeading" : "Load funds on your card instantly",
                        "lblDescription" : "Click on Load Money to load funds and start using your Cashier Card."],
                       ["imgName" : "mb_11" ,
                        "lblHeading" : "Dashboard",
                        "lblDescription" : "Click on the Navigation Button to access the Statement of Transaction, Redeem Coupons, Generate Physical Cards, Customer Support etc."],
                       ["imgName" : "mb_12" ,
                        "lblHeading" : "Statements of Transaction",
                        "lblDescription" : "Click on Statement of Transaction to view transaction summary and see where you have spent your money."],
                       ["imgName" : "Asset 17" ,
                        "lblHeading" : "Activate your Physical Card",
                        "lblDescription" : "Click on Physical Card to apply for your Cashier Card, you need to Activate Physical Card and enter your Kit Number available on the back side of your Card."],
                       ["imgName" : "Asset 19" ,
                        "lblHeading" : "#AbCashNahiCashierChalega",
                        "lblDescription" : "Use your Cashier Card for the payment online and offline at MasterCard merchants counters."]]
        collectionViewTutorial.reloadData()
    }
    
    func adjustButtonsOnScroll(_ nextItem: Int)
    {
        if nextItem == 6
        {
            btnNext.isHidden = true
            btnFinish.isHidden = false
        }
        else
        {
            btnNext.isHidden = false
            btnFinish.isHidden = true
        }
        if nextItem < 1
        {
            btnCancel.isHidden = false
            btnBack.isHidden = true
        }
        else
        {
            btnCancel.isHidden = true
            btnBack.isHidden = false
            
        }
    }
    
    func moveToLoginScreen()
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "loginRegisterVC") as! LoginRegisterVC
        self.present(nextToRegister, animated:true, completion:nil)
    }
}

extension TutorialViewC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate
{
    //MARK: - UICollectionView Datasource & Delegate Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrTutorial.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTutorial", for: indexPath) as! CellTutorial
        cell.setUserInterfaceForTutorial(dictData: arrTutorial[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize:CGSize = CGSize(width: self.collectionViewTutorial.frame.size.width, height: self.collectionViewTutorial.frame.size.height)
        return cellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        if pageControl.currentPage == 0
        {
            btnCancel.isHidden = false
            btnBack.isHidden = true
        }
        else
        {
            btnCancel.isHidden = true
            btnBack.isHidden = false
            
        }
        if pageControl.currentPage == 6
        {
            btnNext.isHidden = true
            btnFinish.isHidden = false
        }
        else
        {
            btnNext.isHidden = false
            btnFinish.isHidden = true
        }
    }
}
