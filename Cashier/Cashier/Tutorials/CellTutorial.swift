//
//  CellTutorial.swift
//  Cashier
//
//  Created by MSEWA on 17/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class CellTutorial: UICollectionViewCell
{
    @IBOutlet weak var imgViewTutorial: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    //MARK: Populating data in CollectionviewCell
    func setUserInterfaceForTutorial(dictData:[String: Any])
    {
        lblHeading.text = dictData["lblHeading"] as? String
        lblDescription.text = dictData["lblDescription"] as? String
        imgViewTutorial.image = UIImage.init(named: dictData["imgName"] as! String)
    }

}
