//
//  ShareVC.swift
//  Cashier
//
//  Created by MSewa on 02/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class ShareVC: UIViewController {

    @IBOutlet weak var nav_View: UIView!
    @IBOutlet weak var back_Btn: UIButton!
    
    @IBOutlet weak var main_View: UIView!
    
    @IBOutlet weak var share_Btn: UIButton!
    var activityViewController:UIActivityViewController?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnGrdntBackground()

        
    }
    @IBAction func bck_Tap(_ sender: Any) {
        [self.dismiss(animated: true, completion: nil)]
    }
    
    @IBAction func share_Tap(_ sender: Any) {
        
        let someText:String = "Hey!! I just Started using CASHIER App and its awesome.You should try using it as well!! Click here to download the app:"
        let objectsToShare:URL = URL(string: "http://www.google.com")!
        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.accessibilityActivate()
        
       // activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook,UIActivityType.postToTwitter,UIActivityType.mail]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func btnGrdntBackground(){
        
        let gradieGet:CAGradientLayer = CAGradientLayer()
        
        let colorTo = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBot = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradieGet.colors = [colorBot, colorTo]
        
        gradieGet.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradieGet.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradieGet.frame = share_Btn.bounds
        
        gradieGet.cornerRadius = 10
        
        share_Btn.layer.insertSublayer(gradieGet, at: 0)
    }
    
}
