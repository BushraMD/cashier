//
//  CustomerSupportVC.swift
//  Cashier
//
//  Created by MSewa on 03/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class CustomerSupportVC: UIViewController {

    @IBOutlet weak var back_Btn: UIButton!
    @IBOutlet weak var nav_View: UIView!
    @IBOutlet weak var super_View: UIView!
    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var viewThree: UIView!
    @IBOutlet weak var viewFour: UIView!
    @IBOutlet weak var butnOne: UIButton!
    @IBOutlet weak var butnTwo: UIButton!
    @IBOutlet weak var butnThree: UIButton!
    @IBOutlet weak var butnFour: UIButton!
    @IBOutlet weak var imgOne: UIImageView!
    @IBOutlet weak var imgTwo: UIImageView!
    @IBOutlet weak var imgThr: UIImageView!
    @IBOutlet weak var imgFour: UIImageView!
    @IBOutlet weak var lblOne: UILabel!
    @IBOutlet weak var lblTwo: UILabel!
    @IBOutlet weak var lblThr: UILabel!
    @IBOutlet weak var lblFour: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGradientBackground()

    }

    func setGradientBackground() {
        let colorTop =  UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        //  gradientLayer.locations = [ 0.0, 0.10]
        gradientLayer.locations = [ 0.5, 0.8]
        
        gradientLayer.frame = self.super_View.bounds
        self.super_View.layer.addSublayer(gradientLayer)
        
        
    }

    @IBAction func back_Tap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        }
    
    
    @IBAction func butnOne_Tap(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let queriesViewC = storyBoard.instantiateViewController(withIdentifier: "QueriesViewC") as! QueriesViewC
        self.present(queriesViewC, animated:true, completion:nil)
    }
    
    
    @IBAction func butntwo_Tap(_ sender: Any)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let queriesViewC = storyBoard.instantiateViewController(withIdentifier: "ReportIssuesViewC") as! ReportIssuesViewC
        self.present(queriesViewC, animated:true, completion:nil)
    }
    
    @IBAction func butnThree_Tap(_ sender: Any) {
        if let url = URL(string: "tel://\(7259195449)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    @IBAction func butn_Tap(_ sender: Any) {
        
        let email = "Care@madmoveglobal.com"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
        
        
        
    }
    
}
