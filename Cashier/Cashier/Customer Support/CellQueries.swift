//
//  CellQueries.swift
//  Cashier
//
//  Created by MSEWA on 16/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class CellQueries: UITableViewCell,UITextViewDelegate
{
    @IBOutlet weak var lblRefNum: UILabel!
    @IBOutlet weak var lblCardType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var txtViewDesc: UITextView!
    @IBOutlet weak var lblClosedOrOpen: UILabel!
    @IBOutlet weak var viewQuery: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        viewQuery.layer.cornerRadius = 5
        self.txtViewDesc.delegate = self
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setUserInterfaceForQueryList(dictData:[String: Any], queryType:String)
    {
        if queryType == "All"
        {
            lblClosedOrOpen.isHidden = false
        }
        else
        {
            lblClosedOrOpen.isHidden = true
        }
        let strRefNo = dictData["ticketNo"] as? String ?? ""
        lblRefNum.text = "RefNo.:" + strRefNo
        lblCardType.text = dictData["component"] as? String ?? ""
        lblClosedOrOpen.text = dictData["status"] as? String ?? ""
        txtViewDesc.text = dictData["description"] as? String ?? ""


        
        let dateDouble = dictData["created"] as? Double ?? 0.0
        let date = Date(timeIntervalSince1970: (dateDouble / 1000.0))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss z"
        let strDate = dateFormatter.string(from: date)
        let newDate = dateFormatter.date(from: strDate)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let newStrDate = dateFormatter.string(from: newDate!)
        lblDate.text = newStrDate

    }
    
    //MARK: UITextView Delegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        return false
    }
    
}
