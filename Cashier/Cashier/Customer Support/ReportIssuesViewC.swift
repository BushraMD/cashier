//
//  ReportIssuesViewC.swift
//  Cashier
//
//  Created by MSEWA on 16/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class ReportIssuesViewC: BaseViewC,UITextViewDelegate,UITextFieldDelegate
{
    @IBOutlet weak var txtIssueType: UITextField!
    @IBOutlet weak var txtViewIssueDesc: UITextView!
    @IBOutlet weak var viewPicker: UIView!
    @IBOutlet weak var pickerViewIssue: UIPickerView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var arrQueryTypeList = [Dictionary<String,Any>]()
    var transactionID = Int()

    //MARK: View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: IBAction Methods
    @IBAction func tapBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapSelectQueryType(_ sender: UIButton)
    {
        viewPicker.isHidden = false
        pickerViewIssue.isHidden = false
    }
    
    @IBAction func tapSubmit(_ sender: UIButton)
    {
        self.view.endEditing(true)
        apiCreateQuery()
    }
    
    //MARK: Private Methods
    func setUpView()
    {
        self.setCornerRadius(sender: btnSubmit, radius: 5.0)
        self.viewPicker.isHidden = true
        apiLoadQueryTypeList()
    }
    
    //MARK: UITextField Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.viewPicker.isHidden = false
        return false
    }

}

extension ReportIssuesViewC
{
    //MARK: API IssueTypeList
    func apiLoadQueryTypeList()
    {
        var jsonString = String()
        
        let requestParams = ["merchantCode": kMerchantCode,
                             "projectCode":kProjectCode] as [String : Any]
        
        print(requestParams)
        
        do {
            let jsonData =  try JSONSerialization.data(withJSONObject: requestParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: URL_QUERYTYPE_LIST, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            guard let code = response["code"] as? String else { return }
            
            if code == "S00"
            {
                guard let arr = response["details"] as? [Dictionary<String,AnyObject>]
                    else {
                        API_Service.alert(applicationName, message: "No Information found!", view: self)
                        return;
                }
                self.arrQueryTypeList = arr
                DispatchQueue.main.async {
                    self.pickerViewIssue.reloadAllComponents()
                    self.txtIssueType.text = self.arrQueryTypeList[0]["name"] as! String
                }
            }
            else
            {
                print("fail------")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
            }
        }
    }
    
    //MARK: API Report Query
    func apiCreateQuery()
    {
        var jsonString = String()

        let requestParams = ["merchantCode": kMerchantCode,
                             "projectCode":kProjectCode,
                             "email":kAppDelegate.email,
                             "mobileNo":kAppDelegate.mobileNumber,
                             "requester":kAppDelegate.firstName,
                             "componentName":txtIssueType.text ?? "",
                             "description":txtViewIssueDesc.text,
                             "transactionId":String(transactionID)] as [String : Any]
        
        print(requestParams)
        
        do {
            let jsonData =  try JSONSerialization.data(withJSONObject: requestParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: URL_REPORT_QUERY, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")

            guard let code = response["code"] as? String else { return }
            
            if code == "S00"
            {
                let alert = UIAlertController(title: kConstAppName, message: "Query submitted successfully.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)            }
            else
            {
                print("fail------")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

            }
        }
    }
    
}

extension ReportIssuesViewC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: PickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return arrQueryTypeList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return arrQueryTypeList[row]["name"] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtIssueType.text = arrQueryTypeList[row]["name"] as? String
        transactionID = (arrQueryTypeList[row]["id"] as? Int)!
        print(transactionID)
        viewPicker.isHidden = true
    }
}
