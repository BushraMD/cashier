//
//  AllQueriesViewC.swift
//  Cashier
//
//  Created by MSEWA on 15/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class AllQueriesViewC: BaseViewC {

    @IBOutlet weak var tblViewAllQueries: UITableView!
    
    var arrAllQueries = [Dictionary<String,Any>]()
    
    //MARK: View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func reloadTableView()
    {
        tblViewAllQueries.reloadData()
    }
    
    //MARK: Private Methods
    func setUpView()
    {
        self.tblViewAllQueries.register(UINib.init(nibName: "CellQueries", bundle: nil), forCellReuseIdentifier: "cellQueries")
        apiAllQueries()
    }

}

extension AllQueriesViewC: UITableViewDelegate, UITableViewDataSource
{
    //MARK:- UITableView Datasource & Delegate Methods
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrAllQueries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CellQueries = tblViewAllQueries.dequeueReusableCell(withIdentifier:
            "cellQueries") as! CellQueries
        cell.selectionStyle = .none
        cell.setUserInterfaceForQueryList(dictData: arrAllQueries[indexPath.row], queryType: "All")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 165
    }
}

extension AllQueriesViewC
{
    //MARK: API CALLS
    func apiAllQueries()
    {
        var jsonString = String()
        
        let requestParams = ["merchantCode": kMerchantCode,
                             "projectCode":kProjectCode,
                             "email":kAppDelegate.email] as [String : Any]
        
        print(requestParams)
        
        do {
            let jsonData =  try JSONSerialization.data(withJSONObject: requestParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: URL_QUERIES, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            guard let code = response["code"] as? String else { return }

            if code == "S00"
            {
                guard let arrDetails = response["details"] as? [Dictionary<String,Any>]
                else
                {
                    let alert = UIAlertController(title: kConstAppName, message: "No Information found!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                        self.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                    return;
                }
                self.arrAllQueries = arrDetails
                DispatchQueue.main.async {
                    self.tblViewAllQueries.reloadData()
                }
                print("success")
            }
            else
            {
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
            }
        }
    }
}
