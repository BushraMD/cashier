//
//  QueriesViewC.swift
//  Cashier
//
//  Created by MSEWA on 15/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit

class QueriesViewC: UIViewController,UIGestureRecognizerDelegate
{

    @IBOutlet weak var lblOpenIssues: UILabel!
    @IBOutlet weak var lblAllIssues: UILabel!
    @IBOutlet weak var containerViewCustomerSupport: UIView!
    
    var objAllQueries: AllQueriesViewC?
    var objOpenQueries: OpenQueriesViewC?
    var currentVC: UIViewController?
    var selectedSegmentIndex = NSInteger()
    var swipeGesture = UISwipeGestureRecognizer()
    
    //MARK: View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: IBAction Methods
    
    @IBAction func tapBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapAllIssues(_ sender: UIButton)
    {
        lblAllIssues.isHidden = false
        lblOpenIssues.isHidden = true
        if currentVC != objAllQueries
        {
            self.view.isUserInteractionEnabled = true
            addChildViewController(objAllQueries!)
            move(toNewController: objAllQueries!)
        }
    }
    
    @IBAction func tapOpenIssues(_ sender: UIButton)
    {
        lblAllIssues.isHidden = true
        lblOpenIssues.isHidden = false
        if currentVC != objOpenQueries {
            self.view.isUserInteractionEnabled = true
            addChildViewController(objOpenQueries!)
            move(toNewController: objOpenQueries!)
        }
    }
    
    //MARK: Private Methods
    func setUpView()
    {
        let direction: [UISwipeGestureRecognizerDirection] = [.up, .down, .left, .right]
        for dir in direction{
            swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeView(_:)))
            swipeGesture.delegate = self
            swipeGesture.cancelsTouchesInView = true
            self.view.addGestureRecognizer(swipeGesture)
            swipeGesture.direction = dir
            self.view.isUserInteractionEnabled = true
            self.view.isMultipleTouchEnabled = true
        }
        
        selectedSegmentIndex = 0
        objAllQueries = (childViewControllers[0] as? AllQueriesViewC)
        lblOpenIssues.isHidden = true
        
        objAllQueries?.reloadTableView()
        
        currentVC = objAllQueries
        objAllQueries?.view.frame = containerViewCustomerSupport.bounds
        objAllQueries = kAppDelegate.storyBoardMain?.instantiateViewController(withIdentifier: "AllQueriesViewC") as? AllQueriesViewC

        objOpenQueries = kAppDelegate.storyBoardMain?.instantiateViewController(withIdentifier: "OpenQueriesViewC") as? OpenQueriesViewC
    }
    
    func move(toNewController newController: UIViewController)
    {
        currentVC?.willMove(toParentViewController: nil)
        
        transition(from: currentVC!, to: newController, duration: 0.6, options: .curveLinear, animations: nil, completion: {(_ finished: Bool) -> Void in
            self.currentVC?.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
            self.currentVC = newController
            self.view.isUserInteractionEnabled = true
        })
    }
    
    @objc func swipeView(_ sender:UISwipeGestureRecognizer)
    {
        UIView.animate(withDuration: 1.0) {
            if sender.direction == .right
            {
                self.lblAllIssues.isHidden = false
                self.lblOpenIssues.isHidden = true
                if self.currentVC != self.objAllQueries
                {
                    self.view.isUserInteractionEnabled = true
                    self.addChildViewController(self.objAllQueries!)
                    self.move(toNewController: self.objAllQueries!)
                }
            }
            else if sender.direction == .left
            {
                self.lblAllIssues.isHidden = true
                self.lblOpenIssues.isHidden = false
                if self.currentVC != self.objOpenQueries {
                    self.view.isUserInteractionEnabled = true
                    self.addChildViewController(self.objOpenQueries!)
                    self.move(toNewController: self.objOpenQueries!)
                }
            }
        }
    }
    
    //MARK: UIGestureRecognizer Delegate
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        return true
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        return true
    }
}
