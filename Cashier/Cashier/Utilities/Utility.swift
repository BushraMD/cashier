//
//  Utility.swift
//  Cashier
//
//  Created by MSEWA on 30/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit

class Utility: NSObject
{
    //MARK: - Function to add check for valid password
    class func isValidPassword(password:String?) -> Bool
    {
        if(password == nil){return false}
        
        if(password!.count < 6){return false}
        
        if(!self.isContainAtleastOneSpecialCharacter(string: password!)){return false}
        
        if(!self.isContaintAtleastOneCapitalLetter(string: password!)){return false}
        
//        if(!self.isContaintAtleastOneNumber(string: password!)){return false}
        
        return true
    }
    
    class func isContainAtleastOneSpecialCharacter(string:String?) -> Bool
    {
        if(string == nil){return false}
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: string!)
        return specialresult
    }
    
    class func isContaintAtleastOneCapitalLetter(string:String?) -> Bool
    {
        if(string == nil){return false}
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: string!)
        return capitalresult
    }
    
    
}
