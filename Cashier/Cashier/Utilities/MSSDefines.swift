//
//  MSSDefines.swift
//  Cashier
//
//  Created by MSEWA on 29/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit

class MSSDefines: NSObject
{
}
    /**
     *  Macros
     */

    let ISIPAD                                  = UIDevice.current.userInterfaceIdiom == .pad
    let ISIPHONE                                = UIDevice.current.userInterfaceIdiom == .phone
    let ISUNSP                                  = UIDevice.current.userInterfaceIdiom == .unspecified
    
    let ISIPHONE4                               = UIScreen.main.bounds.size.height == 480.0 ? true : false
    let ISIPHONE5                               = UIScreen.main.bounds.size.height == 568.0 ? true : false
    let ISIPHONE6                               = UIScreen.main.bounds.size.height == 667.0 ? true : false
    let ISIPHONE6PLUS                           = UIScreen.main.bounds.size.height == 736.0 ? true : false
    
    let kAppDelegate                            = UIApplication.shared.delegate as! AppDelegate
    
    let kUserDefaults                           = UserDefaults.standard
    
    let kDeviceSize                             = UIScreen.main.bounds
    let kDeviceWidth                            = UIScreen.main.bounds.size.width

/**
 *  Constants
 */

let kConstAppName                           = "Cashier"
let kConstAppVersion                        = "1.0"
let kProjectCode                            =   "LETSPRO32301"
let kMerchantCode                           =   "LETS10031"

/**
 *  Storyboard Name
 */

let kStoryboardMain                         = "Main"

/**
 *  User Default Keys
 */

let kUdDeviceToken                                    = "DeviceToken"
let kUdIsLogin                                        = "IsLogin"
let kUdUserId                                         = "UserId"
let kUdFirstName                                      = "FirstName"
let kUdLastName                                       = "LastName"
let kUdEmailId                                        = "EmailId"
let kUdMobileNo                                       = "MobileNo"
let kUDIsTutorialDone                                 = "IsTutorial"

