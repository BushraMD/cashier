//
//  AppDelegate.swift
//  Cashier
//
//  Created by MSEWA on 25/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mobileNumber:String = ""
    var firstName:String = ""
    var lastName:String = ""
    var doB:String = ""
    var email:String = ""
    var password:String = ""
    var idType:String = ""
    var idNo:String = ""
    var sessionId:String = ""
    var storyBoardMain:UIStoryboard?

    

    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        
        DispatchQueue.main.async {
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
        IQKeyboardManager.sharedManager().shouldResignOnTouchOutside = true
        }
        storyBoardMain = UIStoryboard(name: "Main", bundle: nil)

        let isTutorialCompleted = kUserDefaults.value(forKey: kUDIsTutorialDone) as? Bool ?? false
        if isTutorialCompleted
        {
            self.configureLoginRegisterAsRoot()
        }
        else
        {
            self.configureTutorialAsRoot()
        }
//        DispatchQueue.main.async {
//
//        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewControlleripad : LoginRegisterVC = mainStoryboardIpad.instantiateViewController(withIdentifier: "loginRegisterVC") as! LoginRegisterVC
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        self.window?.rootViewController = initialViewControlleripad
//        self.window?.makeKeyAndVisible()
//        }
        return true

    }
    
    func configureLoginRegisterAsRoot()
    {
        let loginRegVC = storyBoardMain?.instantiateViewController(withIdentifier: "loginRegisterVC") as! LoginRegisterVC
        self.window?.rootViewController = loginRegVC
        self.window?.makeKeyAndVisible()
    }

    func configureTutorialAsRoot()
    {
        let tutorialVC = storyBoardMain?.instantiateViewController(withIdentifier: "TutorialViewC") as! TutorialViewC
        self.window?.rootViewController = tutorialVC
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

