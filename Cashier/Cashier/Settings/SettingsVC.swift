//
//  SettingsVC.swift
//  Cashier
//
//  Created by MSewa on 03/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet weak var nav_View: UIView!
    
    @IBOutlet weak var bck_Btn: UIButton!
    
    @IBOutlet weak var password_View: UIView!
    @IBOutlet weak var paswrd_Btn: UIButton!
    
    @IBOutlet weak var notification_Viuew: UIView!
    @IBOutlet weak var chck_Btn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func change_Paswrd(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "ChangePaswrdVC") as! ChangePaswrdVC
        self.present(nextToRegister, animated:true, completion:nil)
    }
    
    @IBAction func chk_Tap(_ sender: Any) {
    }
    @IBAction func back_Tap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
