//
//  PhysicalCardVC.swift
//  Cashier
//
//  Created by MSewa on 05/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class PhysicalCardVC: BaseViewC,UITableViewDelegate,UITableViewDataSource {
  
    
   
  
    @IBOutlet weak var tableDetails: UITableView!
    
    var arrTransactionDetails = [[String : Any]]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableDetails.delegate = self
        self.tableDetails.dataSource = self
        self.tableDetails.tableFooterView = UIView()
        tableDetails.register(UINib(nibName: "TransactionDetails", bundle: nil), forCellReuseIdentifier: "Cell")
        
       physicaTransactionAPICalling()

    }
    
    
    
    func physicaTransactionAPICalling()  {

        self.showActivityIndicatory(uiView: self.view)
        
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: getTransactions, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
            }
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                DispatchQueue.main.async {
                    
                    
                    guard let VirtualDetails = response["physicalCardTransactions"] as? String
                        else { return }
                    //  print("virtualTransactions==>>\(VirtualDetails)")
                    
                    
                    let physicalData = API_Service.getjsonResponse(dataStg: VirtualDetails)
                    
                    if let array = physicalData["transactions"] as? NSArray {
                        //  print("virtualTransactions data == \(array)")
                        
                        self.arrTransactionDetails = array as! [[String:Any]]
                        print("PHYSICAL data == \(self.arrTransactionDetails)")
                        self.tableDetails.reloadData()
                        
                    }
                }
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
            }
            
        }
        

    }
   
    
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
   
    
    
    func convertToDictionary(from text: String) throws -> [String: String] {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: String] ?? [:]
    }
   


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.arrTransactionDetails.count
     //   return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableDetails.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TransactionDetails
        //physical as wel
        if let price = self.arrTransactionDetails [indexPath.row]["balance"] as? String {
            cell.amountLbl.text = "-₹\(price)"
        }
        
        let dateData = Date.UTCToLocalOnlyTime(date:self.arrTransactionDetails [indexPath.row]["date"] as! String )
        
        cell.dateLbl.text = dateData
        
        if let cardId = self.arrTransactionDetails [indexPath.row]["id"] as? String {
            
            cell.idLbl.text = cardId
            
        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
        
    }
    

}




extension String {
    var unescaped: String {
        let entities = ["\0", "\t", "\n", "\r", "\"", "\'", "\\"]
        var current = self
        for entity in entities {
            let descriptionCharacters = entity.debugDescription.characters.dropFirst().dropLast()
            let description = String(descriptionCharacters)
            current = current.replacingOccurrences(of: description, with: entity)
        }
        return current
    }
}















/*
extension PhysicalCardVC: UITableViewDelegate, UITableViewDataSource {
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTransactionDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableDetails.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TransactionDetails
        
//        let transactionobj = self.arrTransactionDetails[indexPath.row]
//        
//        if let transactionDetails = transactionobj["details"] as? [String : Any] {
//            
//            if let merchantname = transactionDetails["merchantname"] as? String {
//                print("merchantname==>>\(merchantname)")
//            } else if let desc = transactionobj["description"] as? String {
//                print("desc==>>\(desc)")
//            }
//        }
         return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == SECTION_INDEX {
//            return 60
//        } else {
//            return UITableViewAutomaticDimension
//        }
//    }
        return 100
        
    }
 */




