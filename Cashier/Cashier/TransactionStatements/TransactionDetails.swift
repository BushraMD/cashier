//
//  TransactionDetails.swift
//  Cashier
//
//  Created by MSewa on 05/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class TransactionDetails: UITableViewCell {
    
    
    @IBOutlet weak var sub_View: UIView!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var idLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
