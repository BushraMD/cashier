//
//  TransactionStatmentVC.swift
//  Cashier
//
//  Created by MSewa on 04/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class TransactionStatmentVC: UIViewController,SlidingContainerViewControllerDelegate {

    @IBOutlet weak var viewPresent: UIView!
    
    @IBOutlet weak var nav_View: UIView!
    
    @IBOutlet weak var back_Btn: UIButton!
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller1 = storyboard.instantiateViewController(withIdentifier: "PhysicalCardVC") as? PhysicalCardVC
        let controller2 = storyboard.instantiateViewController(withIdentifier: "VirtualCardVC") as? VirtualCardVC
        
        
        
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: [controller1!,controller2!],
            titles: ["PHYSICAL CARD", "VIRTUAL CARD"])
        
        
        
        
        self.viewPresent.addSubview(slidingContainerViewController.view)
        slidingContainerViewController.sliderView.topAnchor.constraint(equalTo: view.bottomAnchor,
                                                                       constant: 0).isActive=true
        slidingContainerViewController.sliderView.appearance.outerPadding = 0
        slidingContainerViewController.sliderView.appearance.innerPadding = 50
        slidingContainerViewController.sliderView.appearance.fixedWidth = true
        slidingContainerViewController.setCurrentViewControllerAtIndex(0)
        
        
        
    }
    // MARK: SlidingContainerViewControllerDelegate
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    @objc func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController) {
        
    }
    
    @objc func slidingContainerViewControllerDidMoveToViewControllerAtIndex(_ slidingContainerViewController: SlidingContainerViewController, index: Int) {
        
    }
  
    
    @IBAction func back_Tap(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
