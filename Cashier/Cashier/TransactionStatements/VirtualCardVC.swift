//
//  VirtualCardVC.swift
//  Cashier
//
//  Created by MSewa on 05/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class VirtualCardVC: BaseViewC,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableDetails: UITableView!
    
    var arrTransactionDetails = [[String : Any]]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableDetails.delegate = self
        self.tableDetails.dataSource = self
        self.tableDetails.tableFooterView = UIView()
        tableDetails.register(UINib(nibName: "TransactionDetails", bundle: nil), forCellReuseIdentifier: "Cell")
        self.virtualTransactionAPICalling()
        
    }

    func virtualTransactionAPICalling()  {
        
        self.showActivityIndicatory(uiView: self.view)
        
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: getTransactions, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
            }
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                DispatchQueue.main.async {
                    guard let VirtualDetails = response["virtualTransactions"] as? String
                        else { return }
                  //  print("virtualTransactions==>>\(VirtualDetails)")
                    
                    
                    let physicalData = API_Service.getjsonResponse(dataStg: VirtualDetails)
                    
                    if let array = physicalData["transactions"] as? NSArray {
                      //  print("virtualTransactions data == \(array)")
                        
                        self.arrTransactionDetails = array as! [[String:Any]]
                         print("virtualTransactions data == \(self.arrTransactionDetails)")
                        self.tableDetails.reloadData()
                        
                    }
                }
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
            }
            
        }
    
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTransactionDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableDetails.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TransactionDetails
    //physical as wel
        if let price = self.arrTransactionDetails [indexPath.row]["balance"] as? String {
            cell.amountLbl.text = "-₹\(price)"
        }
    
        let dateData = Date.UTCToLocalOnlyTime(date:self.arrTransactionDetails [indexPath.row]["date"] as! String )
        
        cell.dateLbl.text = dateData
        
        if let cardId = self.arrTransactionDetails [indexPath.row]["id"] as? String {
            
                cell.idLbl.text = cardId
           
        }
       
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 100
        
    }
}
extension Date
{
    static func UTCToLocalOnlyTime(date:String) -> String {
        let dateFormator = DateFormatter()
        //  dateFormator.dateFormat = "yyyy-MM-dd'T'h:mm:ss"
        dateFormator.dateFormat = "yyyy-MM-dd'T'h:mm:ss+hh:mm"
        //            dateFormator.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormator.date(from: date)
        dateFormator.timeZone = TimeZone.current
        dateFormator.dateFormat = "yyyy-MM-dd"
        return dateFormator.string(from: dt!)
    }
}
