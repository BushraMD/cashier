//
//  UserModel.swift
//  Cashier
//
//  Created by MSewa on 02/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

private class SingletonSetupHelper {
    
    var userSessionId: String?
    var userName: String?
    var userType: String?
    var lastName: String?
    var password: String?
    var loadMoneySecretKey: String?
    var loadMoneyTransactionRefNo: String?
    var loadMoneyAuthRefNo: String?
    var loadMoneyAmount: String?

  
    //Get UserDetails
    var firstName: String?
    var balance: Int?
    var dateOfBirth: String?
    var email: String?


}

class UserModel {
    static let shared = UserModel()
    private static let setup = SingletonSetupHelper()
    
    var userSessionId: String!
    var userName: String!
    var userType: String!
    var lastName: String!
    var password: String!
    var dateOfBirth: String?
    var email: String!

    var loadMoneySecretKey: String!
    var loadMoneyTransactionRefNo: String!
    var loadMoneyAuthRefNo: String!
    var loadMoneyAmount: String!
    var firstName: String!
    var balance: Int!

    //Get UserDetails
    
    
    
    
    
    class func setup(userSessionId: String,userName: String,userType: String,lastName: String,loadMoneySecretKey: String, loadMoneyTransactionRefNo: String, loadMoneyAuthRefNo: String, loadMoneyAmount: String,password: String,firstName: String,balance: Int,dateOfBirth: String,email: String){
        

        UserModel.setup.userSessionId = userSessionId
        UserModel.setup.userName = userName
        UserModel.setup.userType = userType
        UserModel.setup.lastName = lastName

        UserModel.setup.password = password

        UserModel.setup.loadMoneySecretKey = loadMoneySecretKey
        UserModel.setup.loadMoneyTransactionRefNo = loadMoneyTransactionRefNo
        UserModel.setup.loadMoneyAuthRefNo = loadMoneyAuthRefNo
        UserModel.setup.loadMoneyAmount = loadMoneyAmount
        
         UserModel.setup.firstName = firstName
        
        UserModel.setup.balance = balance
        UserModel.setup.dateOfBirth = dateOfBirth
        UserModel.setup.email = email

        
    }
    
    func clear() {
        UserModel.shared.userSessionId = nil
        UserModel.shared.userName = nil
        UserModel.shared.userType = nil
        UserModel.shared.lastName = nil
        UserModel.shared.password = nil
        UserModel.shared.loadMoneySecretKey = nil
        UserModel.shared.loadMoneyTransactionRefNo = nil
        UserModel.shared.loadMoneyAuthRefNo = nil
        UserModel.shared.loadMoneyAmount = nil
        UserModel.shared.firstName = nil
        UserModel.shared.balance = nil
        UserModel.setup.dateOfBirth = nil
        UserModel.setup.email = nil

    }
    
    func getSessionId() -> String{
        return userSessionId!
    }
    func getUserName() -> String{
        return userName!
    }
    func getUserType() -> String{
        return userType!
    }
}
