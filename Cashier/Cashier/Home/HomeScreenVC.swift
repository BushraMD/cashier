//
//  HomeScreenVC.swift
//  Cashier
//
//  Created by Apple on 25/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit
import ImageSlideshow



class HomeScreenVC: BaseViewC, SendIndexDelegate,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //return
        let cell = tableDetails.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        
      //  cell.textLabel?.text = "Section \(indexPath.section) Row \(indexPath.row)"
        
        return cell
    }
    

    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var welcmeLbl: UILabel!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var balance_Lbl: UILabel!
    
    @IBOutlet weak var balance_Display_Lbl: UILabel!
    
    @IBOutlet weak var scrl: UIScrollView!

    @IBOutlet weak var tableDetails: UITableView!
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var scrollImges: UIScrollView!
    
    @IBOutlet weak var cardNumbr: UILabel!
    
    @IBOutlet weak var validNumbr: UILabel!
    
    @IBOutlet weak var cardHolderName: UILabel!
    @IBOutlet weak var cvvNumber: UILabel!
    var waletNumbr = String()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var images = [UIImage]()

    var sideMenuBtn = UIButton()
    var transDown = CATransition()
     var rightSideMenu = SideMenuView()
    var mystr: String?
    
    
    @IBOutlet weak var sliderViewTopSwipe: ImageSlideshow!

    
    var dataArray :String!
    var dic : NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // scrl.contentSize.height=1000

       self.topView.isHidden = true
        sideMenuBtn.tag = 1
       getUserDetls()
      fetchVirtualApiCalling()
//      getBalanceApiCalling()
        generateVirtualCardApiCalling()
        
        sliderViewTopSwipe.backgroundColor = UIColor.white
        sliderViewTopSwipe.slideshowInterval = 5.0
        sliderViewTopSwipe.contentScaleMode = UIViewContentMode.scaleToFill
        
        sliderViewTopSwipe.activityIndicator = DefaultActivityIndicator()
        sliderViewTopSwipe.currentPageChanged = { page in
            
            
            
        }
        
        sliderViewTopSwipe.setImageInputs([
            ImageSource(image: UIImage(named: "ob_2.png")!),
            ImageSource(image: UIImage(named: "ob_3.png")!),
            ImageSource(image: UIImage(named: "ob_5.png")!),
            ImageSource(image: UIImage(named: "ob_10.png")!),
            ImageSource(image: UIImage(named: "ob_3.png")!)

            ])
        
        
      

    
//        for i in 0..<images.count {
//            let imageView = UIImageView()
//            let x = self.scrollImges.frame.size.width * CGFloat(i)
//            imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.width, height: self.scrollImges.frame.height)
//            imageView.contentMode = .scaleAspectFit
//            imageView.image = images[i]
//
//            scrollImges.contentSize.width = scrollImges.frame.size.width * CGFloat(i + 1)
//            scrollImges.addSubview(imageView)
//        }
//
//
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        
        getBalanceApiCalling()
        getUserDetls()
        fetchVirtualApiCalling()

    }
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func sideMenu(_ sender: Any) {
        
        if sideMenuBtn.tag == 0 {
            
            hidePopUpView()
            
        } else {
            
            rightSideMenu = Bundle.main.loadNibNamed("SideMenuViewXib", owner: self, options: nil)?[0] as! UIView as! SideMenuView
            rightSideMenu.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
            rightSideMenu.delegate = self
            showPopUpView()
            
        }
    }
    
    @IBAction func tapLoadMoney(_ sender: UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let loadMoneyVC = storyBoard.instantiateViewController(withIdentifier: "LoadMoneyViewC") as! LoadMoneyViewC
        self.present(loadMoneyVC, animated:true, completion:nil)
    }
    
    
    func showPopUpView() {
        
        sideMenuBtn.tag = 1
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFromLeft
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightSideMenu.transform = CGAffineTransform(scaleX: 1, y: 1)
        rightSideMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        rightSideMenu.layer.add(transDown, forKey: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hidePopUpView))
        rightSideMenu.bgView.addGestureRecognizer(tap)
        self.view.addSubview(rightSideMenu)
        
        //====> top view show as background
        self.topView.isHidden = false
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFade
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.topView.transform = CGAffineTransform(scaleX: 1, y: 1)
        topView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.6)
        topView.layer.add(transDown, forKey: nil)
        
        
    }
    
    @objc func hidePopUpView() {   // To hide the PopUp
        
        self.topView.isHidden = true
        
        sideMenuBtn.tag = 1
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFromRight
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightSideMenu.layer.add(transDown, forKey: nil)
        rightSideMenu.isHidden = true
        view.endEditing(true)
        
    }
    
    func sendIndex(_ row : Int){

     topView.isHidden = true
    sideMenuBtn.tag = 1
        
        switch row {
            
        case 0:
            
            break
        case 1:
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "PhysicaCardVController") as! PhysicaCardVController
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 2:
//            let cardObj = self.storyboard?.instantiateViewController(withIdentifier: "shareVC") as! ShareVC
//            self.navigationController?.pushViewController(cardObj, animated: true)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "shareVC") as! ShareVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 3:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToTrn = storyBoard.instantiateViewController(withIdentifier: "TransactionStatmentVC") as! TransactionStatmentVC
            self.present(nextToTrn, animated:true, completion:nil)
            break
        case 4:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "ReddeemVC") as! ReddeemVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 5:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 6:
            
            break
        case 7:
            logOut()

            break
        case 8:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "CustomerSupportVC") as! CustomerSupportVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
            
            
//        case 9:
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "CustomerSupportVC") as! CustomerSupportVC
//            self.present(nextToRegister, animated:true, completion:nil)
//            break
            
        default:
            break
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getUserDetls()  {


        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]

        do {

            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!

        } catch let myJSONError {
            print(myJSONError)
        }

        API_Service.postUrlSession(urlString: getUserDetailsUrl, params: jsonString) { (response) in

            print("UserDetails response =====>>>> \(response)")

            guard let code = response["code"] as? String else { return }

            if code == "S00" {

                DispatchQueue.main.async {

                    guard let details = response["details"] as? [String:Any] else { return }
                    let accountDetails = details["accountDetail"] as! [String:Any]
                     let accountType = accountDetails["accountType"] as! [String:Any]
                      let userDetails = details["userDetail"] as! [String:Any]
                    let nameofUser = accountType["name"] as! String
                    let frstName = userDetails["firstName"] as! String
                    let lastName = userDetails["lastName"] as! String
                    
                   if  let dateOfBrth = userDetails["dateOfBirth"] as? String {
                        UserModel.shared.dateOfBirth = dateOfBrth

                    }
                    if  let email = userDetails["email"] as? String {
                        UserModel.shared.email = email
                        
                    }

                    if let balanceDet = accountDetails["balance"] as? Int {
                         UserModel.shared.balance = balanceDet

                        
                    }

                UserModel.shared.userType = nameofUser
                UserModel.shared.userName = frstName
                UserModel.shared.lastName = lastName
               // UserModel.shared.balance = balanceDet.do

                self.userNameLbl.text! = UserModel.shared.userName
               // self.balance_Lbl.text! = UserModel.shared.balance
                    
                    
                    
                    
                }

            }

            else {

                print("Something went wrong!")

            }

        }
    }
    func logOut()  {
        
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: logOutUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let nextToLog = storyBoard.instantiateViewController(withIdentifier: "loginRegisterVC") as! LoginRegisterVC
                    self.present(nextToLog, animated:true, completion:nil)
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
    }
   func fetchVirtualApiCalling()  {
        
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: fetchVirtualCard, params: jsonString) { (response) in
            
            print("Fetchresponse =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
//                if let waletNum = response["walletNumber"] as? String {
//                    print(waletNum)
//                    var stringNum = String(describing: waletNum)
//
//                    self.cardNumbr.text! = stringNum
//                }
                DispatchQueue.main.async {
                    
                
                if let waletNum = response["walletNumber"] as? String {
                    self.cardNumbr.text = waletNum
                }
                
                if let VALID = response["expiryDate"] as? String {
                    print(VALID)
                    self.validNumbr.text = "valid "+VALID
                }
                
                if let CVV = response["cvv"] as? String {
                    print(CVV)
                      self.cvvNumber.text = "cvv "+CVV
                }
                if let holderName = response["holderName"] as? String {
                    print(holderName)
                    self.cardHolderName.text! = holderName
                }

                }} else if code == "F00"{
                 if let errMsg = response["message"] as? String {
                    print(errMsg)
                }}
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
    }
    
    
    func getBalanceApiCalling()  {
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: getBalance, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    if let balanceTotal = response["balance"] as? Double {
                        print(balanceTotal)
                        self.balance_Display_Lbl.text = String(describing: balanceTotal)
                    }
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
    }
    
    
    
    
    func generateVirtualCardApiCalling()  {
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: generateVirtualCard, params: jsonString) { (response) in
            
            print("Balance response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    
                    
                }
                
            }else if code == "F00"{
                self.mystr = response["message"] as? String
                API_Service.alert(applicationName, message: self.mystr!, view: self)
            }
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
    }
    

}
