//
//  BaseViewC.swift
//  Cashier
//
//  Created by MSEWA on 26/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit

class BaseViewC: UIViewController {
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    func showActivityIndicatory(uiView: UIView) {
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0xffffff, alpha: 0.3)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor(red: (195/255), green: (233/255), blue: (227/255), alpha: 1)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
        
        loadingView.addSubview(activityIndicator)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator(uiView: UIView) {
        
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        //print("Function Name -->",#function)
        //print("Class Name -->",NSStringFromClass(self.dynamicType))
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
    }
    
    
    
    //MARK: - function to get IndexPath

    func getIndexPathFor(sender : AnyObject, tblView : UITableView) -> NSIndexPath?
    {
        let rect = sender.convert(sender.bounds.origin, to: tblView)
        if let indexPath = tblView.indexPathForRow(at: rect)
        {
            return indexPath as NSIndexPath
        }
        return nil
    }
    
    func getIndexPathForCollection(sender : AnyObject, collectionView : UICollectionView) -> NSIndexPath?
    {
        let rect = sender.convert(sender.bounds.origin, to: collectionView)
        if let indexPath = collectionView.indexPathForItem(at: rect)
        {
            return indexPath as NSIndexPath
        }
        return nil
    }
    
    //MARK: - Fuction to set corner radius
    func setCornerRadius(sender : AnyObject, radius : CGFloat)
    {
        sender.layer.cornerRadius = radius
    }
    
    //MARK: - Function to set border
    func setBorder(sender : AnyObject, borderWidth : CGFloat, borderColor: CGColor)
    {
        sender.layer.borderColor = borderColor
        sender.layer.borderWidth = borderWidth
    }
    
    //MARK: - Function to add Done toolbar
    func addDoneToolBar(onTextField textfield:UITextField, target:Any, selector:Selector)
    {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: selector)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textfield.inputAccessoryView = toolBar
    }

    //MARK: - Function to add check for valid email
    class func isValidEmail(emailString:String, strictFilter:Bool) -> Bool {
        
        let strictEmailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString = ".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*"
        let emailRegex = strictFilter ? strictEmailRegEx : laxString
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: emailString)
    }
    
    //MARK: - Function to add check for valid string
    class func validString(string: Any?) -> String
    {
        let str: String? = string as? String
        
        if(str == nil)
        {
            return ""
        }
        else if(str == "<null>" || str == "<NULL>")
        {
            return ""
        }
        else if(str == "<nil>" || str == "<NIL>")
        {
            return ""
        }
        else if(str == "null" || str == "NULL")
        {
            return ""
        }
        else if(str == "NIL" || str == "nil")
        {
            return ""
        }
        else if(str == "(null)")
        {
            return ""
        }
        return str!
    }
    
    //MARK: - Function to add check for valid mobile number
    class func isValidMobileNumber(str:String?) -> Bool
    {
        if(str == nil){return false}
        
        if(str!.count < 4){return false}
        if(str!.count > 13){return false}
        
        return true
    }
    
    
    class func showAlertViewController(viewController:UIViewController, withAlertTitle title:String? , withAlertMessage message: String?)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        { (result : UIAlertAction) -> Void in
        }
        alertController.addAction(okAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    
    
  
}
