//
//  LoginVC.swift
//  Cashier
//
//  Created by MSEWA on 25/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}

class LoginVC: BaseViewC,UITextFieldDelegate,UIGestureRecognizerDelegate
{

   
    @IBOutlet weak var tfView1: UIView!
    @IBOutlet weak var tfView2: UIView!
    @IBOutlet weak var logo_view: UIView!
    @IBOutlet weak var login: UIButton!
  
    @IBOutlet weak var regen_tf_View: UIView!
    
    @IBOutlet weak var enterMblNumberTf: UITextField!
    @IBOutlet weak var passwrdTf: UITextField!
    
    @IBOutlet var forgotPasswordView: UIView!
    
    @IBOutlet weak var forgotPass_SubView: UIView!
    
    @IBOutlet weak var retrivepass_Lbl: UILabel!
    
    @IBOutlet weak var enter_Number_View: UIView!
    
    @IBOutlet weak var flagImg: UIImageView!
    
    @IBOutlet weak var ForgPass_Mbl_Tf: UITextField!

    @IBOutlet weak var generateOtpBtn: UIButton!
    
    @IBOutlet var Forgot_OTP_View: UIView!
    
    @IBOutlet weak var waitingOtp_Lbl: UILabel!
    
    @IBOutlet weak var otpTxtFeildView: UIView!
    
    @IBOutlet weak var tf1: UITextField!
    
    @IBOutlet weak var tf2: UITextField!
    
    @IBOutlet weak var tf3: UITextField!
    
    @IBOutlet weak var tf4: UITextField!
    
    @IBOutlet weak var tf5: UITextField!
    
    @IBOutlet weak var tf6: UITextField!
    
    
    @IBOutlet weak var textF1: UITextField!
    
    @IBOutlet weak var textF2: UITextField!
    
    @IBOutlet weak var textF3: UITextField!
    
    @IBOutlet weak var textF4: UITextField!
    
    @IBOutlet weak var textF5: UITextField!
    
    @IBOutlet weak var textF6: UITextField!
    
    @IBOutlet weak var resentOtp_Btn: UIButton!
    
    @IBOutlet weak var reenter_Mbl_Number_Btn: UIButton!
    
    @IBOutlet weak var submtOtp_Btn: UIButton!
    
   
    @IBOutlet var forgotPass_PasswordView: UIView!
    
    @IBOutlet weak var changePass_View: UIView!
    
    @IBOutlet weak var textFeildView1: UIView!
    
    
    @IBOutlet weak var textFeildView2: UIView!
    
    @IBOutlet weak var newPasswrd_textfeild1: UITextField!
    
    @IBOutlet weak var reenterNewPassTextFeild2: UITextField!
    
    @IBOutlet weak var changePasswrd_Btn: UIButton!
    
    
    @IBOutlet var successView: UIView!
    
    @IBOutlet weak var success_Main_View: UIView!
    
    @IBOutlet weak var sucessMsg_Lbl: UILabel!
    
    @IBOutlet weak var getItNow_Btn: UIButton!
    
    @IBOutlet weak var tryLtr_Btn: UIButton!
    
    @IBOutlet weak var waitingForOtp_View: UIView!
    
    
    
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    
    //MARK: - View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(tapVC))
        forgotPasswordView.addGestureRecognizer(tap)
     
        tap.delegate = self
        //forgot_View.isHidden = true
        btnGrdntBackground()
        designVws()
        txtFeldVwsDesgns()
        addChildViews()
        textFeldsDesigns()
        viewDesigns()
        
      
        
    }
    
    
    override func viewDidLayoutSubviews() {
       // retrive_Passwrd_Gradient_lbl.textColor = UIColor(patternImage:partialGradient(forViewSize: retrive_Passwrd_Gradient_lbl.frame.size, proportion: 0.65))
    }
    
    
    
    @IBAction func gernrate_Otp_Tap(_ sender: Any) {
        self.Forgot_OTP_View.isHidden = false

    }
    
    @IBAction func forgot_Tap(_ sender: Any) {
      //  forgot_View.isHidden = false
//        self.forgotPasswordView.isHidden = false
//   self.forgotPass_SubView.isHidden = false
        
        DispatchQueue.main.async {
            self.forgotPasswordView.alpha = 0
            self.forgotPasswordView.isHidden = false
            UIView.animate(withDuration: 0.4) {
                self.forgotPasswordView.alpha = 1
                
            }
        }
        
        
    }
    
    @IBAction func log_Tap(_ sender: Any) {
       

        if self.enterMblNumberTf.text?.count == 10
        {
            if self.passwrdTf.text == ""
            {
                API_Service.alert(applicationName, message: "Please Enter Password", view: self)
                return
            }; if self.passwrdTf.text?.count == 6{
                 self.loginAPICalling()
            }else{
                API_Service.alert(applicationName, message: "Please Enter 6 Digit  Password", view: self)
            }
//            self.loginAPICalling()
            
        }else
        {
            API_Service.alert(applicationName, message: "Please Enter 10Digit Mobile Number", view: self)
            return
        }
        
        

    }
   
    func loginAPICalling(){
        
        
        if Reachability.isConnectedToNetwork() {
            
       self.showActivityIndicatory(uiView: self.view)
        self.appDelegate.mobileNumber = self.enterMblNumberTf.text!
        self.appDelegate.password = self.passwrdTf.text!
        
        
        let deviceid = UIDevice.current.identifierForVendor?.uuidString
        
        var jsonString = String()
            
        let reqDict = ["username":self.appDelegate.mobileNumber,"password":self.appDelegate.password,"registrationId":"","ipAddress":deviceid] as [String : Any]
            
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        

//            API_Service.getUrlSession(url: URL(string: "your url here")!) { (response) in
//
//            }
            
        API_Service.postUrlSession(urlString: loginUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            DispatchQueue.main.async {
                self.hideActivityIndicator(uiView: self.view)
            }
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                DispatchQueue.main.async {
//                    self.hideActivityIndicator(uiView: self.view)
                    UserModel.shared.password = self.passwrdTf.text
                    let dic = response["details"] as! [String:Any]

                    if let sessionId = dic["sessionId"] as? String {
                    UserModel.shared.userSessionId = sessionId
                    }
                    

                    let sessionId = dic["sessionId"] as? String
                    let userDetails = dic["userDetail"] as! [String:Any]

                    let email = userDetails["email"] as? String
                    let firstName = userDetails["firstName"] as? String
                    UserModel.shared.userSessionId = sessionId

                    kAppDelegate.email = email ?? ""
                    kAppDelegate.firstName = firstName ?? ""

                    self.fetchUrlCard()
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
                    self.present(nextToRegister, animated:true, completion:nil)
                   
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                 API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
            }
            }
        } else {
            print("please check your internet connection.")
        }
    }
    
    @IBAction func generateOTP_Tap(_ sender: Any) {
       
       // self.forgotPasswordView.isHidden = true

        if self.ForgPass_Mbl_Tf.text?.count == 10
        {
                    self.generateOTPAPICalling()
        }
        else
        {
            API_Service.alert(applicationName, message: "Please Enter 10Digit Mobile Number", view: self)
            return
        }
        
        
        
    
        
       }
    
    func generateOTPAPICalling()  {
        
        //self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        let reqDict = ["username":self.ForgPass_Mbl_Tf.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: forgotPasswrdUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                self.forgotPasswordView.isHidden = true
                DispatchQueue.main.async {
                    self.hideActivityIndicator(uiView: self.view)
                    self.Forgot_OTP_View.isHidden = false
                    
                    
    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

                
            }
            
        }
    }
    
    
    
    @IBAction func submitOTP_Tap(_ sender: Any) {
        self.forgotPass_PasswordView.isHidden = false
        self.Forgot_OTP_View.isHidden = true

    

    
    }
    
    @IBAction func resend_Otp_Tap(_ sender: Any) {
        
        self.resendOTPAPICaliing()
        
     
    }
    func resendOTPAPICaliing()  {
       // self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        var reqDict = [String: Any]()
        reqDict["mobileNumber"] = self.ForgPass_Mbl_Tf.text
        
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: resendChngeOtpUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    self.hideActivityIndicator(uiView: self.view)
                    // self.successView.isHidden = false
                    
                    
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
    }
    
    @IBAction func reEnterMbl_Tap(_ sender: Any) {
        self.forgotPasswordView.isHidden = false

    }
    
    
    @IBAction func change_Passwrd_tap(_ sender: Any) {
        self.forgotPass_PasswordView.isHidden = true
        
        self.changePasswrd()
       
       
    }
    
    
    func changePasswrd() {
        
       // self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        
        let reqDict = ["username":self.ForgPass_Mbl_Tf.text!,"key":textF1.text!+textF2.text!+textF3.text!+textF4.text!+textF5.text!+textF6.text!,"password":self.newPasswrd_textfeild1.text!,"confirmPassword":self.reenterNewPassTextFeild2.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: changePasswrdUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                self.hideActivityIndicator(uiView: self.view)
                DispatchQueue.main.async {
                    
                    self.successView.isHidden = false
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

                
            }
            
        }
        
    }
    
    @IBAction func tryIt_Now_Tap(_ sender: Any) {
        self.successView.isHidden = true
        self.forgotPasswordView.isHidden = false

    }
    
    
    
    @IBAction func try_Ltr_Tap(_ sender: Any) {
    }
    
    
    
    func partialGradient(forViewSize size: CGSize, proportion p: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        
        
        context?.setFillColor(UIColor.darkGray.cgColor)
        context?.fill(CGRect(origin: .zero, size: size))
        
        let c1 = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let c2 = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let top = CGPoint(x: 0, y: size.height * (1.0 - p))
        let bottom = CGPoint(x: 0, y: size.height)
        
        let colorspace = CGColorSpaceCreateDeviceRGB()
        
        if let gradient = CGGradient(colorsSpace: colorspace, colors: [c2, c1] as CFArray, locations: [0.0, 0.1]){
            // change 0.0 above to 1-p if you want the top of the gradient orange
            context?.drawLinearGradient(gradient, start: top, end: bottom, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        }
        
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    
    func btnGrdntBackground(){
        
//        let gradientOne:CAGradientLayer = CAGradientLayer()
//
//        let colorTopBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
//
//        let colorBottomBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
//
//        gradientOne.colors = [colorTopBtn, colorBottomBtn]
//
//        gradientOne.startPoint = CGPoint(x: 0.0, y: 0.5)
//
//        gradientOne.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//       gradientOne.frame = generateOtpBtn.bounds
//
//        gradientOne.cornerRadius = 10
//
//       generateOtpBtn.layer.insertSublayer(gradientOne, at: 0)
//
        
//        let gradientBtn:CAGradientLayer = CAGradientLayer()
//        
//        let colorTopGoogleBtnn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
//        
//        let colorBottomGoogleBtnn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
//        self.forgotPass_SubView.addSubview(generateOtpBtn)
//        gradientBtn.colors = [colorBottomGoogleBtnn, colorTopGoogleBtnn]
//        
//        gradientBtn.startPoint = CGPoint(x: 0.0, y: 0.5)
//        
//        gradientBtn.endPoint = CGPoint(x: 1.0, y: 0.5)
//        
//        gradientBtn.frame = self.generateOtpBtn.bounds
//        
//        gradientBtn.cornerRadius = 10
        
       // generateOtpBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //generateOtpBtn.layer.insertSublayer(gradientBtn, at: 0)
     //   self.generateOtpBtn.layer.addSublayer(gradientBtn)

        
        let gradientOn:CAGradientLayer = CAGradientLayer()
        
        let colorTopBt = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomBt = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradientOn.colors = [colorBottomBt, colorTopBt]
        
        gradientOn.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradientOn.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradientOn.frame = login.bounds
        
        gradientOn.cornerRadius = 10
        
        login.layer.insertSublayer(gradientOn, at: 0)
        
        
        
        //Button Gradient Color
        
        let gradient:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradient.frame = login.bounds
        
        gradient.cornerRadius = 10
        
        login.layer.insertSublayer(gradient, at: 0)
        
        
        //Button Gradient Color
        
        let gradientn:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBt = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBt = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradientn.colors = [colorBottomGoogleBt, colorTopGoogleBt]
        
        gradientn.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradientn.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        //gradientn.frame = generateOtpBtn.bounds
        
        gradientn.cornerRadius = 10
        
       // generateOtpBtn.layer.insertSublayer(gradientn, at: 0)
        
        //Button Gradient Color
        
        let gradien:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogle = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogle = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradien.colors = [colorBottomGoogle, colorTopGoogle]
        
        gradien.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradien.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradien.frame = submtOtp_Btn.bounds
        
        gradien.cornerRadius = 10
        
        submtOtp_Btn.layer.insertSublayer(gradien, at: 0)
        
        
        
        //Button Gradient Color
        
        let gradienPass:CAGradientLayer = CAGradientLayer()
        
        let colorTop = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradienPass.colors = [colorBottom, colorTop]
        
        gradienPass.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradienPass.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradienPass.frame = changePasswrd_Btn.bounds
        
        gradienPass.cornerRadius = 10
        
        changePasswrd_Btn.layer.insertSublayer(gradienPass, at: 0)
        
        
        
        //Button Gradient Color
        
        let gradieGet:CAGradientLayer = CAGradientLayer()
        
        let colorTo = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBot = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradieGet.colors = [colorBot, colorTo]
        
        gradieGet.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradieGet.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradieGet.frame = getItNow_Btn.bounds
        
        gradieGet.cornerRadius = 10
        
        getItNow_Btn.layer.insertSublayer(gradieGet, at: 0)
        
    
        
    }
    
    func designVws(){
        
        
        let pi = CGFloat(Float.pi)
        let start:CGFloat = 0.0
        let end :CGFloat = pi
        
        // circlecurve
        let path: UIBezierPath = UIBezierPath();
        path.addArc(
            withCenter: CGPoint(x:self.logo_view.frame.width/2, y:self.logo_view.frame.height/8),
            radius: 300,
            startAngle: start,
            endAngle: end,
            clockwise: true
        )
        
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor.gray.cgColor
        layer.path = path.cgPath
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 2.0
        layer.shadowRadius = 2.0
        self.logo_view.layer.addSublayer(layer)
        
        
        //Forgot Password View Design
//        regenrate_otp_View.layer.cornerRadius = 15
//        regenrate_otp_View.layer.shadowColor = UIColor.darkGray.cgColor
//        regenrate_otp_View.layer.shadowOffset = CGSize.zero
//        regenrate_otp_View.layer.shadowOpacity = 2.0
//        regenrate_otp_View.layer.shadowRadius = 2.0
//        regenrate_otp_View.layer.masksToBounds =  false
//        regenrate_otp_View.layer.backgroundColor = UIColor.white.cgColor
        
//        regen_tf_View.layer.shadowOpacity = 0.7
//        regen_tf_View.layer.shadowRadius = 5
        
    
    }
    func txtFeldVwsDesgns(){
        
        
        self.enterMblNumberTf.delegate = self
        //tfView1
        tfView1.layer.cornerRadius = 15
        tfView1.layer.shadowColor = UIColor.darkGray.cgColor
        tfView1.layer.shadowOffset = CGSize.zero
        tfView1.layer.shadowOpacity = 2.0
        tfView1.layer.shadowRadius = 2.0
        tfView1.layer.masksToBounds =  false
        tfView1.layer.backgroundColor = UIColor.white.cgColor
        
        //tfView2
        
        tfView2.layer.cornerRadius = 15
        tfView2.layer.shadowColor = UIColor.darkGray.cgColor
        tfView2.layer.shadowOffset = CGSize.zero
        tfView2.layer.shadowOpacity = 2.0
        tfView2.layer.shadowRadius = 2.0
        tfView2.layer.masksToBounds =  false
        tfView2.layer.backgroundColor = UIColor.white.cgColor
    }
    
    func separtView() {
      
        
    }
    
    func viewDesigns() {
        //Retrived Password
        
        forgotPass_SubView.layer.cornerRadius = 15
        forgotPass_SubView.layer.shadowColor = UIColor.darkGray.cgColor
        forgotPass_SubView.layer.shadowOffset = CGSize.zero
        forgotPass_SubView.layer.shadowOpacity = 2.0
        forgotPass_SubView.layer.shadowRadius = 2.0
        forgotPass_SubView.layer.masksToBounds =  false
        forgotPass_SubView.layer.backgroundColor = UIColor.white.cgColor
        waitingForOtp_View.layer.cornerRadius = 15
        waitingForOtp_View.layer.shadowColor = UIColor.darkGray.cgColor
        waitingForOtp_View.layer.shadowOffset = CGSize.zero
        waitingForOtp_View.layer.shadowOpacity = 2.0
        waitingForOtp_View.layer.shadowRadius = 2.0
        waitingForOtp_View.layer.masksToBounds =  false
        waitingForOtp_View.layer.backgroundColor = UIColor.white.cgColor
        
        
        enter_Number_View.layer.shadowOpacity = 0.7
        enter_Number_View.layer.shadowRadius = 5
        
        changePass_View.layer.cornerRadius = 15
        changePass_View.layer.shadowColor = UIColor.darkGray.cgColor
        changePass_View.layer.shadowOffset = CGSize.zero
        changePass_View.layer.shadowOpacity = 2.0
        changePass_View.layer.shadowRadius = 2.0
        changePass_View.layer.masksToBounds =  false
        changePass_View.layer.backgroundColor = UIColor.white.cgColor
        
        textFeildView1.layer.shadowOpacity = 0.7
        textFeildView1.layer.shadowRadius = 5
        
        textFeildView2.layer.shadowOpacity = 0.7
        textFeildView2.layer.shadowRadius = 5
        
        
        success_Main_View.layer.cornerRadius = 15
        success_Main_View.layer.shadowColor = UIColor.darkGray.cgColor
        success_Main_View.layer.shadowOffset = CGSize.zero
        success_Main_View.layer.shadowOpacity = 2.0
        success_Main_View.layer.shadowRadius = 2.0
        success_Main_View.layer.masksToBounds =  false
        success_Main_View.layer.backgroundColor = UIColor.white.cgColor
        
        
        
        
        
    }
    
    func textFeldsDesigns(){
        
        
        textF1.delegate = self as? UITextFieldDelegate
        textF2.delegate = self as? UITextFieldDelegate
        textF3.delegate = self as? UITextFieldDelegate
        textF4.delegate = self as? UITextFieldDelegate
        textF5.delegate = self as? UITextFieldDelegate
        textF6.delegate = self as? UITextFieldDelegate
        
        
        textF1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textF2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textF3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textF4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textF5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        textF6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        
        
        textF1.layer.shadowOpacity = 0.7
        textF1.layer.shadowRadius = 5

        textF2.layer.shadowOpacity = 0.7
        textF2.layer.shadowRadius = 5

        textF3.layer.shadowOpacity = 0.7
        textF3.layer.shadowRadius = 5

        textF4.layer.shadowOpacity = 0.7
        textF4.layer.shadowRadius = 5

        textF5.layer.shadowOpacity = 0.7
        textF5.layer.shadowRadius = 5

        textF6.layer.shadowOpacity = 0.7
        textF6.layer.shadowRadius = 5
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case textF1:
                textF2.becomeFirstResponder()
            case textF2:
                textF3.becomeFirstResponder()
            case textF3:
                textF4.becomeFirstResponder()
            case textF4:
                textF5.becomeFirstResponder()
            case textF5:
                textF6.becomeFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case textF1:
                textF1.becomeFirstResponder()
            case textF2:
                textF1.becomeFirstResponder()
            case textF3:
                textF2.becomeFirstResponder()
            case textF4:
                textF3.becomeFirstResponder()
            case textF5:
                textF4.becomeFirstResponder()
            case textF6:
                textF5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    func addChildViews(){
        
        self.forgotPasswordView.frame = self.view.bounds
        self.view .addSubview(forgotPasswordView)
        self.forgotPasswordView.isHidden = true
        
        
        
        self.Forgot_OTP_View.frame = self.view.bounds
        self.view .addSubview(Forgot_OTP_View)
        self.Forgot_OTP_View.isHidden = true
        
        self.forgotPass_PasswordView.frame = self.view.bounds
        self.view .addSubview(forgotPass_PasswordView)
        self.forgotPass_PasswordView.isHidden = true
        
        self.successView.frame = self.view.bounds
        self.view .addSubview(successView)
        self.successView.isHidden = true
        
    }
  
    func fetchUrlCard()  {
        
        self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: fetchVirtualCard, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                self.hideActivityIndicator(uiView: self.view)
                DispatchQueue.main.async {
                    
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

            }
            
        
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let charsLimit = 10
        let startingLength = textField.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace =  range.length
        let newLength = startingLength + lengthToAdd - lengthToReplace
        return newLength <= charsLimit
        
    
        
    }
    
    
    @IBAction func back_Tap(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @objc func tapVC(gesture: UITapGestureRecognizer) {
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.4, animations: {
                self.forgotPasswordView.alpha = 0
            }) { (finished) in
                
                self.forgotPasswordView.isHidden = finished
            }
        }
    }
    
}
