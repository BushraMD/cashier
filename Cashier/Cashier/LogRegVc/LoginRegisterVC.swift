//
//  LoginRegisterVC.swift
//  Cashier
//
//  Created by MSEWA on 25/12/1939 Saka.
//  Copyright © 1939 Saka MSEWA. All rights reserved.
//

import UIKit

class LoginRegisterVC: BaseViewC
{
    @IBOutlet weak var log_Img: UIImageView!
    @IBOutlet weak var register_Btn: UIButton!
    @IBOutlet weak var login_Btn: UIButton!
    @IBOutlet weak var gradient_View: UIView!
    var gradientLayer: CAGradientLayer!
    
    @IBOutlet weak var banner_Img: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        gradientColorForButtons()
        
        kUserDefaults.set(true, forKey: kUDIsTutorialDone)
        kUserDefaults.synchronize()
      
    }
    
    
    override func viewDidLayoutSubviews() {
        setGradientBackground()
        
    }
    
    
    //MARK:- View Lifecycle Methods
    
    @IBAction func log_Tap(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextToLog = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
        self.present(nextToLog, animated:true, completion:nil)
        
    }
    
    
    @IBAction func register_Tap(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.present(nextToRegister, animated:true, completion:nil)
    }
    
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:- Private Methods
    func createCustomButtons()
    {
        self.setCornerRadius(sender: login_Btn, radius: 10)
        self.setCornerRadius(sender: register_Btn, radius: 10)
    }
    func setGradientBackground() {
        let colorTop =  UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        //  gradientLayer.locations = [ 0.0, 0.10]
        gradientLayer.locations = [ 0.5, 0.8]
        
        gradientLayer.frame = self.gradient_View.bounds
        self.gradient_View.layer.addSublayer(gradientLayer)
        
        
    }
    
    func arcShape(){
        
        
        let shapeSize = gradient_View.frame.size
        gradient_View.backgroundColor = UIColor.white
        
        let path = CGMutablePath()
        path.move(to: CGPoint.zero)
        
        let curveWidthOneFourth = shapeSize.width / 4
        let curveHeight = shapeSize.height * 0.1
        
        path.addCurve(to: CGPoint(x: curveWidthOneFourth * 2, y: curveHeight), control1: .zero, control2: CGPoint(x: curveWidthOneFourth, y: curveHeight))
        path.addCurve(to: CGPoint(x: shapeSize.width, y: 0), control1: CGPoint(x: curveWidthOneFourth * 3, y: curveHeight), control2: CGPoint(x: shapeSize.width, y: 0))
        
        path.addLine(to: CGPoint(x: shapeSize.width, y: shapeSize.height))
        path.addLine(to: CGPoint(x: 0, y: shapeSize.height))
        path.addLine(to: CGPoint.zero)
        
        let layer = CAShapeLayer()
        layer.path = path
        layer.fillColor = UIColor.red.cgColor
        gradient_View.layer.addSublayer(layer)
        
        
    }
    
    
    func gradientColorForButtons(){
        
        let gradient:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradient.frame = login_Btn.bounds
        
        gradient.cornerRadius = 10
        
        login_Btn.layer.insertSublayer(gradient, at: 0)
        
        
        let fbGradient:CAGradientLayer = CAGradientLayer()
        let colorTopFacebookBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottomFacebookBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        fbGradient.colors = [colorBottomFacebookBtn, colorTopFacebookBtn]
        fbGradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        fbGradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        fbGradient.frame = register_Btn.bounds
        fbGradient.cornerRadius = 10
        register_Btn.layer.insertSublayer(fbGradient, at: 0)
        
    }
  
}



