//
//  PhysicaCardVController.swift
//  Cashier
//
//  Created by MSewa on 05/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class PhysicaCardVController: BaseViewC, SendIndexDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate {
   
    

    @IBOutlet var phsicalView: UIView!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var imgVew: UIImageView!
    @IBOutlet weak var main_View: UIView!
    
    @IBOutlet weak var loadMnyView: UIView!
    @IBOutlet weak var addBtn: UIButton!
    
    @IBOutlet weak var backwrd_Btn: UIButton!
    
    @IBOutlet weak var frwrd_Btn: UIButton!
    
    @IBOutlet var loaderrView: UIView!
    
    @IBOutlet var userDetailsView: UIView!
    
    @IBOutlet weak var userMainView: UIView!
    
    @IBOutlet weak var userView: UIView!
    
    @IBOutlet weak var imagOne: UIImageView!
    
    @IBOutlet weak var imagTwo: UIImageView!
    
    @IBOutlet weak var imagThr: UIImageView!
    @IBOutlet weak var userNameTf: UITextField!
    @IBOutlet weak var dobTf: UITextField!
        @IBOutlet weak var emailTf: UITextField!
    
    @IBOutlet weak var userViewBack_Btn: UIButton!
    
    @IBOutlet weak var userForwrdBtn: UIButton!
    
    
    @IBOutlet var addressView: UIView!
    
    @IBOutlet weak var addressMainView: UIView!
    
    @IBOutlet weak var addressDetailsVew: UIView!
    @IBOutlet weak var enterPostalCdeTf: UITextField!
    
    @IBOutlet weak var enterCtyNameTf: UITextField!
    
    @IBOutlet weak var enterStateTf: UITextField!
    
    @IBOutlet weak var flatNumHouseNameTf: UITextField!
    
    @IBOutlet weak var landmrkTf: UITextField!
    
    @IBOutlet weak var addressBckBtn: UIButton!
    
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var topView:UIView!
    
    @IBOutlet weak var getBalnceView: UIView!
    
    
    @IBOutlet weak var decline_Btn: UIButton!
    
    @IBOutlet weak var accept_Btn: UIButton!
    @IBOutlet weak var pgeCntrl: UIPageControl!

    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var userBalLbl: UILabel!
    @IBOutlet weak var loadMony_Btn: UIButton!
    
    @IBOutlet var respons_View: UIView!
    
    @IBOutlet weak var physicalCardRequestResLbl: UILabel!
    
    
    @IBOutlet weak var done_Btn: UIButton!
    
    @IBOutlet var activeCardView: UIView!
    @IBOutlet weak var showImgAcView: UIView!
    @IBOutlet weak var actvImg: UIImageView!
    @IBOutlet weak var addActvBtn: UIButton!
    @IBOutlet var uiKitView: UIView!
    @IBOutlet weak var uiKitTxtFeld: UITextField!
    @IBOutlet weak var activateBtn: UIButton!
    
    @IBOutlet weak var resendOtp_Btn: UIButton!
    @IBOutlet var enterOtpView: UIView!
    @IBOutlet weak var otpVew: UIView!
    @IBOutlet weak var txtFeldView: UIView!
    @IBOutlet weak var tx1: UITextField!
    @IBOutlet weak var tx2: UITextField!
    @IBOutlet weak var tx3: UITextField!
    @IBOutlet weak var tx4: UITextField!
    @IBOutlet weak var tx5: UITextField!
    @IBOutlet weak var tf6: UITextField!
    @IBOutlet weak var submt_Btn: UIButton!
    
    
    
    
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    var sideMenuBtn = UIButton()
     var transDown = CATransition()
     var rightSideMenu = SideMenuView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addressDetailsVew.isHidden = true
        self.topView.isHidden = true
        self.getBalnceView.isHidden = true
        sideMenuBtn.tag = 1
        self.userNameLbl.text = UserModel.shared.userName
        self.respons_View.isHidden = true
        self.activeCardView.isHidden = true
        txtFeldView.layer.shadowOpacity = 0.7
        txtFeldView.layer.shadowRadius = 5
        tx1.delegate = self
        tx2.delegate = self
        tx3.delegate = self
        tx4.delegate = self
        tx5.delegate = self
        tf6.delegate = self

      balnceChkApiCalling()
       displayUserData()

        
      self.vewDesigns()
      
    }
    
  
    @IBAction func backwrd_Tap(_ sender: Any) {
        self.phsicalView.isHidden = false
        self.loaderrView.isHidden = true

    }
    

    @IBAction func frwrd_Tap(_ sender: Any) {
        self.userDetailsView.isHidden = false
        self.loaderrView.isHidden = true

    }
    
    @IBAction func userBack_Tap(_ sender: Any) {
        self.loaderrView.isHidden = false
        self.userDetailsView.isHidden = true

    }
    
    @IBAction func userForwrdTap(_ sender: Any) {
        self.addressDetailsVew.isHidden = false
        self.userDetailsView.isHidden = true
        self.phsicalView.isHidden = true

    }
    
    
    @IBAction func addressBack_Tap(_ sender: Any) {
    
        self.userDetailsView.isHidden = false
        self.addressDetailsVew.isHidden = true

    }
    
    
    
    @IBAction func applyTap(_ sender: Any) {
    self.addressDetailsVew.isHidden = true
    self.main_View.isHidden = true
   //self.respons_View.isHidden = false

    physicalCardReqstApiCalling()
    
    }
    
    
 
    
    func vewDesigns()  {
        self.phsicalView.frame = self.main_View.bounds
        self.main_View .addSubview(phsicalView)
        self.phsicalView.isHidden = false
        
        
  
        self.loaderrView.frame = self.main_View.bounds
        self.main_View .addSubview(loaderrView)
        self.loaderrView.isHidden = true

        
        self.userDetailsView.frame = self.main_View.bounds
    self.main_View .addSubview(userDetailsView)
        self.userDetailsView.isHidden = true
        
        self.respons_View.frame = self.view.bounds
        self.view.addSubview(respons_View)
        self.respons_View.isHidden = true
        
        
        self.uiKitView.frame = self.view.bounds
        self.view.addSubview(uiKitView)
        self.uiKitView.isHidden = true

    
        
        self.enterOtpView.frame = self.view.bounds
        self.view.addSubview(enterOtpView)
        self.enterOtpView.isHidden = true
        
        self.activeCardView.frame = self.view.bounds
        self.view.addSubview(activeCardView)
        self.activeCardView.isHidden = true
        
//        let tap = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
//        tap.delegate = self as? UIGestureRecognizerDelegate
//        enterOtpView.addGestureRecognizer(tap)
//        tap.delegate = self as? UIGestureRecognizerDelegate

//        self.addressDetailsVew.frame = self.main_View.bounds
//        self.main_View .addSubview(addressDetailsVew)
//        self.addressDetailsVew.isHidden = true
        
        
        //View Designs
        subView.layer.shadowOpacity = 0.7
        subView.layer.shadowRadius = 5
        loadMnyView.layer.shadowOpacity = 0.7
        loadMnyView.layer.shadowRadius = 5
        userMainView.layer.shadowOpacity = 0.7
        userMainView.layer.shadowRadius = 5
        addressMainView.layer.shadowOpacity = 0.7
        addressMainView.layer.shadowRadius = 5
    }
    
//    func handleTap(sender: UITapGestureRecognizer? = nil) {
//        self.enterOtpView.isHidden = true
//    }
  
    
    @IBAction func sideMenu(_ sender: Any) {
        if sideMenuBtn.tag == 0 {
            
            hidePopUpView()
            
        } else {
            
            rightSideMenu = Bundle.main.loadNibNamed("SideMenuViewXib", owner: self, options: nil)?[0] as! UIView as! SideMenuView
            rightSideMenu.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
            rightSideMenu.delegate = self
            showPopUpView()
            
        }
    }
    
    
    func showPopUpView() {
        
        sideMenuBtn.tag = 1
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFromLeft
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightSideMenu.transform = CGAffineTransform(scaleX: 1, y: 1)
        rightSideMenu.backgroundColor = UIColor.darkGray.withAlphaComponent(0)
        rightSideMenu.layer.add(transDown, forKey: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hidePopUpView))
        rightSideMenu.bgView.addGestureRecognizer(tap)
        self.view.addSubview(rightSideMenu)
        
        //====> top view show as background
        self.topView.isHidden = false
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFade
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.topView.transform = CGAffineTransform(scaleX: 1, y: 1)
        topView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.6)
        topView.layer.add(transDown, forKey: nil)
        
        
    }
    
    @objc func hidePopUpView() {   // To hide the PopUp
        
        self.topView.isHidden = true
        
        sideMenuBtn.tag = 1
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFromRight
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        rightSideMenu.layer.add(transDown, forKey: nil)
        rightSideMenu.isHidden = true
        view.endEditing(true)
        
    }
    func sendIndex(_ row : Int){
        
        topView.isHidden = true
        sideMenuBtn.tag = 1
        
        switch row {
            
        case 0:
            
            break
        case 1:
            
         
            break
            
        case 2:
            //            let cardObj = self.storyboard?.instantiateViewController(withIdentifier: "shareVC") as! ShareVC
            //            self.navigationController?.pushViewController(cardObj, animated: true)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "shareVC") as! ShareVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 3:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToTrn = storyBoard.instantiateViewController(withIdentifier: "TransactionStatmentVC") as! TransactionStatmentVC
            self.present(nextToTrn, animated:true, completion:nil)
            break
        case 4:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "ReddeemVC") as! ReddeemVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 5:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        case 6:
            
            break
        case 7:
           // logOut()
            break
        case 8:
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "CustomerSupportVC") as! CustomerSupportVC
            self.present(nextToRegister, animated:true, completion:nil)
            break
        default:
            break
        }
    }
    @IBAction func backMe(_ sender: Any) {
        [self.dismiss(animated: true, completion: nil)]
    }
    
    
    
    
    @IBAction func add_Btn(_ sender: Any){

        self.loaderrView.isHidden = false
       balnceChkApiCalling()
        
    }
    
    
    
    
    func balnceChkApiCalling(){
        
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: getBalance, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
            
                if let bal = response["balance"] as? Int {
                    if bal >= 200{
                        DispatchQueue.main.async {
                            
                            if let balNew = response["balance"] as? Int{
                                print(balNew)
                                self.userBalLbl.text = String(describing: balNew)
                            }
                            
                                                self.loaderrView.isHidden = false
                                                    self.getBalnceView.isHidden = true
                            
                            
                            
                            
                            
                            
                            
                                                }
                    }
                    else{
                                            DispatchQueue.main.async {
                        
                                            self.getBalnceView.isHidden = false
                                            }
                                        }
                }
                
                
                
                
                
//                guard let bal = response["balance"] as? Int else { return }
//                if bal == 200{
//                     DispatchQueue.main.async {
//                    self.loaderrView.isHidden = false
//                        self.getBalnceView.isHidden = true
//
//                    }
//                }else{
//                    DispatchQueue.main.async {
//
//                    self.getBalnceView.isHidden = false
//                    }
//                }
                
            }
            else {
                
                print("Something went wrong!")
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    func physicalCardReqstApiCalling() {
        var jsonString = String()
        
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: physicalCardRequst, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    self.respons_View.isHidden = false
                    if let responsMsg = response["message"] as? String{
                        print(responsMsg)
                        self.physicalCardRequestResLbl.text = responsMsg
                
                    }
                    
                }
               
            } else if code == "F00"{
//                self.respons_View.isHidden = false
        DispatchQueue.main.async {
    
          self.respons_View.isHidden = false
            if let responsMsg = response["message"] as? String{
                                    print(responsMsg)
                                    self.physicalCardRequestResLbl.text = responsMsg
                                    }

    
                }
//                DispatchQueue.main.async {
////                self.respons_View.isHidden = false
////                if let responsMsg = response["message"] as? String{
////                    print(responsMsg)
////                    self.physicalCardRequestResLbl.text = responsMsg
////                    }
//
//                }
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
        
    }
    
    @IBAction func decline_Tap(_ sender: Any) {
        self.getBalnceView.isHidden = true
    }
    
    
    
    @IBAction func accept_Tap(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
        self.present(nextToRegister, animated:true, completion:nil)
        
    }
    
    func displayUserData(){
        self.userNameTf.text! = UserModel.shared.userName
        self.dobTf.text = UserModel.shared.dateOfBirth
        
        print("UserModel.shared.dateOfBirth ==>> \n \(UserModel.shared.dateOfBirth)")
        //self.dobTf.text = String(describing: dob)
        self.emailTf.text! = UserModel.shared.email
      //  self.dobTf.text = self.appDelegate.doB

    }
    
    
    @IBAction func loadMoney_Tap(_ sender: Any) {
    }
    
    @IBAction func done_Tap(_ sender: Any) {
        self.respons_View.isHidden = true
      self.activeCardView.isHidden = false

        
    }
    
    @IBAction func add_Tap(_ sender: Any) {
        self.uiKitView.isHidden = false
        self.activeCardView.isHidden = true
        
    
    
    }
    
    func addPhysicardApiCalling() {
        var jsonString = String()
        
        let reqDict = ["sessionId":UserModel.shared.userSessionId,"proxy_number":self.uiKitTxtFeld.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: addPhysicalCard, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    self.enterOtpView.isHidden = false
                    
                }
                
            } else if code == "F00"{
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
        
    }
    @IBAction func activate_Tap(_ sender: Any) {
        self.uiKitView.isHidden = true
          self.enterOtpView.isHidden = false

        addPhysicardApiCalling()
        
    }
    func activatePhysicalCarApiCalling()  {
        var jsonString = String()
        
        let reqDict = ["sessionId":UserModel.shared.userSessionId,"activationCode":tx1.text!+tx2.text!+tx3.text!+tx4.text!+tx5.text!+tf6.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: activatePhysicalCard, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                       // self.resendOtpForPhysicalCard()
                       self.fetchPhysicalCardDetails()
                    
                }
                
            } else if code == "F00"{
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
    }
    func fetchPhysicalCardDetails(){
        
        var jsonString = String()
        
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: fetchPhysicalCard , params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    
                }
                
            } else if code == "F00"{
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
        
        
    }
    
    func resendOtpForPhysicalCard()  {
        
        
        
        var jsonString = String()
        
        let reqDict = ["sessionId":UserModel.shared.userSessionId] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: resendOtpPhysicalCard, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    
                }
                
            } else if code == "F00"{
                
            }
                
            else {
                
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                
                
            }
            
        }
        
        
        
        
        
        
        
        
    }
    
    @IBAction func submt_Tap(_ sender: Any) {
        
        activatePhysicalCarApiCalling()
        
        
    }
    
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case tx1:
                tx2.becomeFirstResponder()
            case tx2:
                tx3.becomeFirstResponder()
            case tx3:
                tx4.becomeFirstResponder()
            case tx4:
                tx5.becomeFirstResponder()
            case tx5:
                tf6.becomeFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case tx1:
                tx1.becomeFirstResponder()
            case tx2:
                tx1.becomeFirstResponder()
            case tx3:
                tx2.becomeFirstResponder()
            case tx4:
                tx3.becomeFirstResponder()
            case tx5:
                tx4.becomeFirstResponder()
            case tf6:
                tx5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
    }
    
    
    @IBAction func resendOtpPhysicard_Tap(_ sender: Any) {
    }
}
