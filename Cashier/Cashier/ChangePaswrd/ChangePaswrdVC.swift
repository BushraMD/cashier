//
//  ChangePaswrdVC.swift
//  Cashier
//
//  Created by MSewa on 03/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class ChangePaswrdVC: UIViewController {
    
    @IBOutlet weak var nav_View: UIView!
    @IBOutlet weak var bck_Btn: UIButton!
    
    
    @IBOutlet weak var enter_Curent_Pswrd_Tf: UITextField!
    
    @IBOutlet weak var enter_new_paswrd_tf: UITextField!
    
    @IBOutlet weak var change_Btn: UIButton!
    
    @IBOutlet weak var diss_miss_Btn: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnGrdntBackground()
    }
    @IBAction func change_Tap(_ sender: Any) {
  
        var jsonString = String()
        let reqDict = ["sessionId":UserModel.shared.userSessionId,"username":self.appDelegate.mobileNumber,"password":UserModel.shared.password,"newPassword":self.enter_new_paswrd_tf.text,"confirmPassword":self.enter_Curent_Pswrd_Tf.text] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: changePasswrd, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                   
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
        
    }
    
    

    @IBAction func back_Tap(_ sender: Any) {
        [self.dismiss(animated: true, completion: nil)];

    }
    
    @IBAction func diss_Miss_Tap(_ sender: Any) {
        [self.dismiss(animated: true, completion: nil)];
    }
    func btnGrdntBackground(){
        
        let gradieGet:CAGradientLayer = CAGradientLayer()
        
        let colorTo = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBot = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradieGet.colors = [colorBot, colorTo]
        
        gradieGet.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradieGet.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradieGet.frame = change_Btn.bounds
        
        gradieGet.cornerRadius = 10
        
        change_Btn.layer.insertSublayer(gradieGet, at: 0)
        
        
        let gradieGe:CAGradientLayer = CAGradientLayer()
        
        let colorT = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBo = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradieGe.colors = [colorBo, colorT]
        
        gradieGe.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradieGe.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradieGe.frame = diss_miss_Btn.bounds
        
        gradieGe.cornerRadius = 10
        
        diss_miss_Btn.layer.insertSublayer(gradieGe, at: 0)
    }
}
