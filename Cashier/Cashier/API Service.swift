//
//  API Service.swift
//  Cashier
//
//  Created by MSewa on 23/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class API_Service: NSObject {
    
    
    class func getUrlSession(url: URL,completion completionHandler:@escaping (_ response: AnyObject) -> ()) {
        
        
        var tempJson: AnyObject?
        let session = URLSession.shared
        
        
        let task = session.dataTask(with: url) { (data, response, error) -> Void in
            
            guard let responseData = data else {
                let errorDes = error?.localizedDescription
                completionHandler(errorDes! as AnyObject)
                return
            }
            
            
            guard error == nil else {
                print(error?.localizedDescription as Any)
                return
            }
            do {
                
                if let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                    tempJson = json
                    completionHandler(tempJson as! NSDictionary)
                    
                } else if let json = try JSONSerialization.jsonObject(with: responseData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray {
                    tempJson = json
                    completionHandler(tempJson as! NSArray)
                }
            }
            catch  {
                let string = "Server is under maintenance. Please try again after sometime."
                completionHandler(string as AnyObject)
            }
        }
        task.resume()
        
    }
    
    
    
    /*!
     * @discussion This function is used to send the request to server with rest api services to get response
     * @param String : here we have to pass urlString value which is of type string
     * @return which returns response value
     */
    
    class func postUrlSession(urlString: String, params: String, completion completionHandler:@escaping (_ response: AnyObject) -> ()) {
        
        
        var tempJson: AnyObject?
        let session = URLSession.shared
        
        let url = NSURL(string:urlString)
        let request = NSMutableURLRequest(url: url! as URL)
        
        request.httpMethod = "POST"
        request.addValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("1222", forHTTPHeaderField: "hash")
        
        request.httpBody = params.data(using: String.Encoding.utf8)
        
        //request.httpBody =  try! JSONSerialization.data(withJSONObject: params, options: [])
        
        //let jsonData = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        //request.httpBody = try! JSONSerialization.jsonObject(with: jsonData, options: []) as! Data
        
        
        // print(request)
        
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            
            
            guard let responseData = data else {
                let errorDes = error?.localizedDescription
                completionHandler(errorDes! as AnyObject)
                return
            }
            
            guard error == nil else {
                
                return
            }
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? NSArray {
                    tempJson = json
                    completionHandler((tempJson!))
                }
                if let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? NSDictionary {
                    tempJson = json
                    completionHandler((tempJson!))
                }
                if let json = try JSONSerialization.jsonObject(with: responseData, options: .allowFragments) as? [String] {
                    tempJson = json as AnyObject?
                    completionHandler((tempJson!))
                }
            } catch {
                
                let string = "Server is under maintenance. Please try again after sometime."
                completionHandler(string as AnyObject)
            }
            
        }
        task.resume()
    }
    
    
    //Alert
    class func alert(_ title : String, message : String, view:UIViewController)
    {
        let alert = UIAlertController(title:title, message:  message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        view.present(alert, animated: true, completion: nil)
    }
    
  class  func getjsonResponse(dataStg : String) -> [String:AnyObject] {
        
        let jsonData = dataStg.data(using: .utf8)
        var jsonResposne : [String:AnyObject]!
        do {
            jsonResposne = try JSONSerialization.jsonObject(with: jsonData!, options: []) as! [String: AnyObject]
            
        }
        catch {
            print("Couldn't parse json \(error)")
        }
        return jsonResposne
        
    }
    

}
