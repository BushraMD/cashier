//
//  RegisterDetailViewController.swift
//  Cashier
//
//  Created by MSewa on 22/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class RegisterDetailViewController: UIViewController,UITextFieldDelegate{

    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    @IBOutlet weak var logo_Img: UIImageView!
    @IBOutlet weak var logo_view: UIView!
    @IBOutlet weak var grad_Lbl: UILabel!
    @IBOutlet weak var tfNameView: UIView!
    @IBOutlet weak var tfdobView: UIView!
    @IBOutlet weak var tfEmailView: UIView!
    @IBOutlet weak var TfPassView: UIView!
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var passwrdTf: UITextField!
    @IBOutlet weak var submt_Btn: UIButton!
    @IBOutlet var lastNameTf: UITextField!
    
    let datePicker = UIDatePicker()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnGradentBackground()
        viewsDesign()
        showDatePicker()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    
    @IBAction func submt_Tap(_ sender: Any) {
        
        
        if fullNameTF.text == "" {
            API_Service.alert(applicationName, message: "Please Enter FirstName", view: self)
        }
        if lastNameTf.text == "" {
             API_Service.alert(applicationName, message: "Please Enter LastName", view: self)
        }
        if dobTF.text == "" {
            API_Service.alert(applicationName, message: "Please Enter DOB", view: self)
        }
        if emailTf.text == "" {
            API_Service.alert(applicationName, message: "Please Enter Email", view: self)
        }
        if passwrdTf.text == "" {
            API_Service.alert(applicationName, message: "Please Provide Password", view: self)
        }
        if (emailTf.text?.isValidEmail())!{
            API_Service.alert(applicationName, message: "Please Provide Valid Email", view: self)
        }
        self.appDelegate.firstName = self.fullNameTF.text!
        self.appDelegate.lastName = self.lastNameTf.text!
        self.appDelegate.doB = self.dobTF.text!
        self.appDelegate.email = self.emailTf.text!
        self.appDelegate.password = self.passwrdTf.text!

        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextToAccessCard = storyBoard.instantiateViewController(withIdentifier: "registerIDVC") as! RegisterIDVC
        self.present(nextToAccessCard, animated:true, completion:nil)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        grad_Lbl.textColor = UIColor(patternImage:partialGradient(forViewSize: grad_Lbl.frame.size, proportion: 0.65))
    }
    
    
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        dobTF.inputAccessoryView = toolbar
        // add datepicker to textField
        dobTF.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        dobTF.text = formatter.string(from: datePicker.date)
        
        valDOB = dobTF.text!
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    
    
    
    
    func partialGradient(forViewSize size: CGSize, proportion p: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        
        
        context?.setFillColor(UIColor.darkGray.cgColor)
        context?.fill(CGRect(origin: .zero, size: size))
        
        let c1 = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let c2 = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let top = CGPoint(x: 0, y: size.height * (1.0 - p))
        let bottom = CGPoint(x: 0, y: size.height)
        
        let colorspace = CGColorSpaceCreateDeviceRGB()
        
        if let gradient = CGGradient(colorsSpace: colorspace, colors: [c2, c1] as CFArray, locations: [0.0, 0.1]){
            // change 0.0 above to 1-p if you want the top of the gradient orange
            context?.drawLinearGradient(gradient, start: top, end: bottom, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        }
        
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    
    func btnGradentBackground()  {
        let gradient:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradient.frame = submt_Btn.bounds
        
        gradient.cornerRadius = 10
        
        submt_Btn.layer.insertSublayer(gradient, at: 0)
    }
    func viewsDesign(){
        let pi = CGFloat(Float.pi)
        let start:CGFloat = 0.0
        let end :CGFloat = pi
        
        // circlecurve
        let path: UIBezierPath = UIBezierPath();
        path.addArc(withCenter: CGPoint(x:self.logo_view.frame.width/2, y:self.logo_view.frame.height/2),
                    radius: 300,
                    startAngle: start,
                    endAngle: end,
                    clockwise: true
        )
        
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor.gray.cgColor
        layer.path = path.cgPath
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 2.0
        layer.shadowRadius = 2.0
        self.logo_view.layer.addSublayer(layer)
        
        tfNameView.layer.shadowOpacity = 0.7
        tfNameView.layer.shadowRadius = 5
        tfdobView.layer.shadowOpacity = 0.7
        tfdobView.layer.shadowRadius = 5
        tfEmailView.layer.shadowOpacity = 0.7
        tfEmailView.layer.shadowRadius = 5
        TfPassView.layer.shadowOpacity = 0.7
        TfPassView.layer.shadowRadius = 5
    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

