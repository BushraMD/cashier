//
//  AccessCardVC.swift
//  Cashier
//
//  Created by MSewa on 22/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class AccessCardVC: BaseViewC {

    @IBOutlet weak var gradientView: UIView!
    
    @IBOutlet weak var grad_Lbl: UILabel!
    @IBOutlet weak var access_Btn: UIButton!
    
    override func viewDidLayoutSubviews() {
        setGradientBackground()
         grad_Lbl.textColor = UIColor(patternImage:partialGradient(forViewSize: grad_Lbl.frame.size, proportion: 0.65))
        
    }
    
    @IBAction func accessCard(_ sender: Any) {
       
        DispatchQueue.main.async {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            
            let nextToAccessCard = storyBoard.instantiateViewController(withIdentifier: "HomeScreenVC") as! HomeScreenVC
            self.present(nextToAccessCard, animated:true, completion:nil)
        
        
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func setGradientBackground() {
        
        access_Btn.layer.cornerRadius = 10
        
        let colorTop =  UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        //  gradientLayer.locations = [ 0.0, 0.10]
        gradientLayer.locations = [ 0.5, 0.8]
        
        gradientLayer.frame = self.gradientView.bounds
        self.gradientView.layer.addSublayer(gradientLayer)
        
        
    }
    func partialGradient(forViewSize size: CGSize, proportion p: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        let context = UIGraphicsGetCurrentContext()
        
        
        context?.setFillColor(UIColor.darkGray.cgColor)
        context?.fill(CGRect(origin: .zero, size: size))
        
        let c1 = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let c2 = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let top = CGPoint(x: 0, y: size.height * (1.0 - p))
        let bottom = CGPoint(x: 0, y: size.height)
        
        let colorspace = CGColorSpaceCreateDeviceRGB()
        
        if let gradient = CGGradient(colorsSpace: colorspace, colors: [c2, c1] as CFArray, locations: [0.0, 0.1]){
            // change 0.0 above to 1-p if you want the top of the gradient orange
            context?.drawLinearGradient(gradient, start: top, end: bottom, options: CGGradientDrawingOptions.drawsAfterEndLocation)
        }
        
        
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
}
