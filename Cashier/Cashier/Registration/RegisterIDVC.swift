//
//  RegisterIDVC.swift
//  Cashier
//
//  Created by MSewa on 26/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class RegisterIDVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate  {
  
    

    @IBOutlet weak var aadharidTF: UITextField!
    @IBOutlet weak var aadharDatePiker: UITextField!
    @IBOutlet weak var aadharView1: UIView!
    @IBOutlet weak var aadharView2: UIView!
    @IBOutlet weak var regBtn: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var pickOption = ["AADHAR", "DRIVING LICENSE", "PAN CARD", "PASSPORT", "VOTER ID"]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewsDesgns()
        btnGradentBackgrnd()
        var pickerView = UIPickerView()
        pickerView.delegate = self
        aadharDatePiker.inputView = pickerView
    }
    
    @IBAction func registerID_Tap(_ sender: Any) {
        
        
        var jsonString = String()
        
      

        let reqDict = ["firstName":self.appDelegate.firstName,"lastName":self.appDelegate.lastName,"dateOfBirth":self.appDelegate.doB,"email":self.appDelegate.email,"password":self.appDelegate.password,"contactNo":self.appDelegate.mobileNumber,"idNo":self.appDelegate.idNo,"idType":self.appDelegate.idType] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: registerFullDetailsUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//                let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "loginVC") as! LoginVC
//                self.present(nextToRegister, animated:true, completion:nil)
//
                
                let mainV = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.presentedViewController?.dismiss(animated: true, completion: nil)
                
                
                
            } else if code == "F00" {
                
                
//                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//                
//                let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "registerDetailViewController") as! RegisterDetailViewController
//                self.present(nextToRegister, animated:true, completion:nil)
                
                
            }
                
                
                
                
                
            else {
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

                print("Something went wrong!")
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        
//        let nextToAccessCard = storyBoard.instantiateViewController(withIdentifier: "accessCardVC") as! AccessCardVC
//        self.present(nextToAccessCard, animated:true, completion:nil)
        
    }
    
    func viewsDesgns(){
        aadharView1.layer.shadowOpacity = 0.7
        aadharView1.layer.shadowRadius = 5
        aadharView2.layer.shadowOpacity = 0.7
        aadharView2.layer.shadowRadius = 5
    }
    
    
    func btnGradentBackgrnd(){
        let gradient:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = regBtn.bounds
        gradient.cornerRadius = 10
        regBtn.layer.insertSublayer(gradient, at: 0)
        
    }

    
    //DatePiker Delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        aadharDatePiker.text = pickOption[row]
    }
    
    
    @IBAction func back_Tap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
