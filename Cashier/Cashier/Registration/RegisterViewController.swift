//
//  RegisterViewController.swift
//  Cashier
//
//  Created by MSewa on 21/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class RegisterViewController: BaseViewC, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var logo_view: UIView!
    @IBOutlet weak var cashier_BckImg: UIImageView!
    @IBOutlet weak var mobileTxtfeild: UITextField!
    @IBOutlet weak var mobile_View: UIView!
    @IBOutlet weak var flag_Img: UIImageView!
    @IBOutlet weak var submt_toProceed: UIButton!
    @IBOutlet weak var otp_Blur_View: UIView!
    @IBOutlet weak var otp_Main_View: UIView!
    @IBOutlet weak var circle_View: UIView!
    @IBOutlet weak var grad_View: UIView!
    @IBOutlet weak var time_Label: UILabel!
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    @IBOutlet weak var tf3: UITextField!
    @IBOutlet weak var tf4: UITextField!
    @IBOutlet weak var tf5: UITextField!
    @IBOutlet weak var tf6: UITextField!
    @IBOutlet var resendOTP_Btn: UIButton!
    @IBOutlet weak var reenterOtp_Btn: UIButton!
    @IBOutlet var logInOtp_Btn: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var errorMsg: String?
    var timer: Timer?
    var count = 120
    
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(tapVC))
        otp_Blur_View.addGestureRecognizer(tap)
        tap.delegate = self
        txtFeildDesignAndDelgts()
        butnGradientBackground()
        viewDesigns()
        //var timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: Selector("update"), userInfo: nil, repeats: true)

     
        
    }
    

    
    
    @IBAction func submt_Tap(_ sender: Any) {
        
        
     //  self.showActivityIndicatory(uiView: self.view)
        
        self.appDelegate.mobileNumber = self.mobileTxtfeild.text!
        
        if(mobileTxtfeild.text == "" )
        {
            API_Service.alert(applicationName, message: "Please enter MobileNumber", view: self)
            print("Should not Empty Feild")
            return
            
        }else if (mobileTxtfeild.text?.count==10){
            
          //  self.showActivityIndicatory(uiView: self.view)
            
            var jsonString = String()
            let reqDict = ["contactNo": mobileTxtfeild.text!, "androidDeviceID": ""] as [String : Any]
            
            do {
                
                let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
                
            } catch let myJSONError {
                print(myJSONError)
            }

            
            
            API_Service.postUrlSession(urlString: registerUrl, params: jsonString) { (response) in

               // self.hideActivityIndicator(uiView: self.view)

                print("response =====>>>> \(response)")
                
                
                guard let code = response["code"] as? String else { return }
                
                if code == "S00" {
                    
                    DispatchQueue.main.async {
                        self.otp_Blur_View.alpha = 0
                        self.otp_Blur_View.isHidden = false
                        UIView.animate(withDuration: 0.4) {
                            self.otp_Blur_View.alpha = 1
                             self.time_Label.text = "\(self.minutesSeconds(seconds: self.count))"
                             self.startTimer()
                        }
                    }
                    
                } else if code == "F00"  {
                  
                   // self.hideActivityIndicator(uiView: self.view)

                    DispatchQueue.main.async {
                        if let  status = response["fullyFilled"] as? Bool{
                            print(status)
                            
                            let regVC = self.storyboard?.instantiateViewController(withIdentifier: "registerDetailViewController") as! RegisterDetailViewController
                            self.presentedViewController?.dismiss(animated: true, completion: nil)
                            
                        }
                        
                        

                        
                        
                        if let message = response["message"] as? String{
                            print(message)
                           self.errorMsg = message
                        
                            
                        }
                       
                        API_Service.alert(applicationName, message:self.errorMsg!, view: self)

                        
                        
                    }

                }

                    
                else {
                    
                    API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

                    print("Something went wrong!")
                    
                }

            }

        }}
    
    @IBAction func logInOtp_Tap(sender: AnyObject) {
        print(#function)
       
        if tf1.text != "" && tf2.text != "" && tf3.text != "" && tf4.text != "" && tf5.text != "" {
            otpApiMethod()
        } else {
            API_Service.alert(applicationName, message: "Please Enter OTP", view: self)
        }
        
      
      
    }
    func otpApiMethod () {
        self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        let reqDict = ["mobileNumber": self.appDelegate.mobileNumber,  "key": tf1.text!+tf2.text!+tf3.text!+tf4.text!+tf5.text!+tf6.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: OtpUrl, params: jsonString) { (response) in
            
            print("OTP response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                     self.hideActivityIndicator(uiView: self.view)
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let nextToRegister = storyBoard.instantiateViewController(withIdentifier: "registerDetailViewController") as! RegisterDetailViewController
                    self.present(nextToRegister, animated:true, completion:nil)
                    
                    
                    
                } }else {
                
                 self.hideActivityIndicator(uiView: self.view)
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
                print("Something went wrong!")
                
            }
            
        }
        
    }
    @IBAction func resendOtp_Tap(sender: AnyObject) {
        
        
        //self.showActivityIndicatory(uiView: self.view)
        var jsonString = String()
        let reqDict = ["mobileNumber": self.appDelegate.mobileNumber] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: resendOtpUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
              //  self.hideActivityIndicator(uiView: self.view)

                
              
                
            } else if code == "F00" {
                
               }
                

            else {
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)

                print("Something went wrong!")
                
            }
    
    }
    }
    
    @IBAction func reenterOtp_Tap(sender: AnyObject) {
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.4, animations: {
                self.otp_Blur_View.alpha = 0
            }) { (finished) in
                
                self.otp_Blur_View.isHidden = finished
            }
        }
        
    }
    
    
    override func viewDidLayoutSubviews() {
        
        setGradientBackground()
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         var maxLength = 1
        if (textField == mobileTxtfeild){
         maxLength = 10
        }else{
          maxLength = 1
        }
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
       // let charsLimit = 10
        
//        let startingLength = textField.text?.characters.count ?? 0
//        let lengthToAdd = string.characters.count
//        let lengthToReplace =  range.length
//        let newLength = startingLength + lengthToAdd - lengthToReplace
//
//        return newLength <= charsLimit
        
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        let text = textField.text
        
        if text?.count == 1{
            switch textField{
            case tf1:
                tf2.becomeFirstResponder()
            case tf2:
                tf3.becomeFirstResponder()
            case tf3:
                tf4.becomeFirstResponder()
            case tf4:
                tf5.becomeFirstResponder()
            case tf5:
                tf6.becomeFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0{
            switch textField{
            case tf1:
                tf1.becomeFirstResponder()
            case tf2:
                tf1.becomeFirstResponder()
            case tf3:
                tf2.becomeFirstResponder()
            case tf4:
                tf3.becomeFirstResponder()
            case tf5:
                tf4.becomeFirstResponder()
            case tf6:
                tf5.becomeFirstResponder()
            default:
                break
            }
            
        }
      /*
        let text = textField.text
        if  text?.count == 1 {
            switch textField{
            case tf1:
                tf2.becomeFirstResponder()
            case tf2:
                tf3.becomeFirstResponder()
            case tf3:
                tf4.becomeFirstResponder()
            case tf4:
                tf5.becomeFirstResponder()
            case tf5:
                tf6.becomeFirstResponder()
            default:
                break
            }
        }
        if  text?.count == 0 {
            switch textField{
            case tf1:
                tf1.becomeFirstResponder()
            case tf2:
                tf1.becomeFirstResponder()
            case tf3:
                tf2.becomeFirstResponder()
            case tf4:
                tf3.becomeFirstResponder()
            case tf5:
                tf4.becomeFirstResponder()
            case tf6:
                tf5.becomeFirstResponder()
            default:
                break
            }
        }
        else{
            
        }
 */
    }
    
  
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        mobileTxtfeild.resignFirstResponder()
        return true
    }
    
    
    func setGradientBackground() {
        let colorTop =  UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        //  gradientLayer.locations = [ 0.0, 0.10]
        gradientLayer.locations = [ 0.5, 0.8]
        
        gradientLayer.frame = self.grad_View.bounds
        self.grad_View.layer.addSublayer(gradientLayer)
        self.grad_View .addSubview(self.time_Label)

        
        
    }
    
    
    
    func txtFeildDesignAndDelgts(){
        tf1.delegate = self
        tf2.delegate = self
        tf3.delegate = self
        tf4.delegate = self
        tf5.delegate = self
        tf6.delegate = self
        
        
        tf1.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        tf2.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        tf3.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        tf4.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        tf5.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        tf6.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingChanged)
        
        
        //TextFeild Style
        tf1.layer.shadowOpacity = 0.7
        tf1.layer.shadowRadius = 5
        
        tf2.layer.shadowOpacity = 0.7
        tf2.layer.shadowRadius = 5
        
        tf3.layer.shadowOpacity = 0.7
        tf3.layer.shadowRadius = 5
        
        tf4.layer.shadowOpacity = 0.7
        tf4.layer.shadowRadius = 5
        
        tf5.layer.shadowOpacity = 0.7
        tf5.layer.shadowRadius = 5
        
        tf6.layer.shadowOpacity = 0.7
        tf6.layer.shadowRadius = 5
        
    }
    
    
    func butnGradientBackground(){
        let gradient:CAGradientLayer = CAGradientLayer()
        
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradient.frame = submt_toProceed.bounds
        
        gradient.cornerRadius = 10
        
        submt_toProceed.layer.insertSublayer(gradient, at: 0)
        
        
//        let gradientBtn:CAGradientLayer = CAGradientLayer()
//
//        let colorTopGoogleBtnn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
//
//        let colorBottomGoogleBtnn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
//
//        gradientBtn.colors = [colorBottomGoogleBtnn, colorTopGoogleBtnn]
//
//        gradientBtn.startPoint = CGPoint(x: 0.0, y: 0.5)
//
//        gradientBtn.endPoint = CGPoint(x: 1.0, y: 0.5)
//
//        gradientBtn.frame = submt_toProceed.bounds
//
//        gradientBtn.cornerRadius = 10
//
//        logInOtp_Btn.layer.insertSublayer(gradientBtn, at: 0)
        
    }
    
    func viewDesigns()  {
        
        otp_Blur_View.isHidden = true
        mobileTxtfeild.delegate = self
        
        // circlecurve
        
        let pi = CGFloat(Float.pi)
        let start:CGFloat = 0.0
        let end :CGFloat = pi
        
        
        
        let path: UIBezierPath = UIBezierPath();
        path.addArc(
            withCenter: CGPoint(x:self.logo_view.frame.width/2, y:self.logo_view.frame.height/8),
            radius: 300,
            startAngle: start,
            endAngle: end,
            clockwise: true
        )
        
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor.gray.cgColor
        layer.path = path.cgPath
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = 2.0
        layer.shadowRadius = 2.0
        self.logo_view.layer.addSublayer(layer)
        
        
        //View Shadow
        
        mobile_View.layer.cornerRadius = 15
        mobile_View.layer.shadowColor = UIColor.darkGray.cgColor
        mobile_View.layer.shadowOffset = CGSize.zero
        mobile_View.layer.shadowOpacity = 2.0
        mobile_View.layer.shadowRadius = 2.0
        mobile_View.layer.masksToBounds =  false
        mobile_View.layer.backgroundColor = UIColor.white.cgColor
        
        
        //Circle_OTP_View
        grad_View.layer.cornerRadius = 80;
        self.grad_View.layer.cornerRadius = self.grad_View.frame.size.width / 2;
        self.grad_View.clipsToBounds = true;
        
        
        
        // grad_View.layer.cornerRadius = grad_View.frame.size.width/2
        circle_View.layer.cornerRadius = circle_View.frame.size.width/2
        
        //OTP View Style
        otp_Main_View.layer.cornerRadius = 15
        otp_Main_View.layer.shadowColor = UIColor.darkGray.cgColor
        otp_Main_View.layer.shadowOffset = CGSize.zero
        otp_Main_View.layer.shadowOpacity = 2.0
        otp_Main_View.layer.shadowRadius = 2.0
        otp_Main_View.layer.masksToBounds =  false
        otp_Main_View.layer.backgroundColor = UIColor.white.cgColor
        
        }
    
    @objc func tapVC(gesture: UITapGestureRecognizer) {
        
        DispatchQueue.main.async {
            
            UIView.animate(withDuration: 0.4, animations: {
                self.otp_Blur_View.alpha = 0
            }) { (finished) in
                self.stopTimer()
                self.otp_Blur_View.isHidden = finished
            }
        }
    }
    func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    
    @objc func loop() {
        count = count-1
        time_Label.text = "\(self.minutesSeconds(seconds: count))"
        resendOTP_Btn.isHidden = true
        reenterOtp_Btn.isHidden = true
        if count == 0 {
            stopTimer()
            resendOTP_Btn.isHidden = false
            reenterOtp_Btn.isHidden = false
        }
    }
    func minutesSeconds (seconds : Int) -> (String) {
        let sec = (seconds % 3600) % 60
        let min = (seconds % 3600) / 60
        let time = "\(min):\(sec)"
        return time
    }

    @IBAction func back_Tap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
