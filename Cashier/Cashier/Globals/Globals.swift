//
//  Globals.swift
//  Cashier
//
//  Created by Apple on 24/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import Foundation
import SystemConfiguration


//let baseUrl = "https://cards.mycashier.in"
let applicationName = "Cashier"
let baseUrl = "http://13.127.101.55/IplCards"
let api = "Api"
let version = "v1"
let role = "User"
let device = "Ios"
let language = "en"
let seprator = "/"
var valDOB = ""
let URL_MAIN_CUSTOMER_SUPPORT = "https://www.letsmanageit.in/Api/V1/"

let registerUrl = baseUrl+seprator+api+seprator+role+seprator+device+seprator+language+seprator+"CustomRegister"
let resendOtpUrl = baseUrl+"/Api/User/Ios/en/Resend/Mobile/OTP"
let OtpUrl = baseUrl+"/Api/User/Android/en/CustomActivate/Mobile"
let registerFullDetailsUrl = baseUrl+"/Api/User/Ios/en/RegisterFullDetails"
let forgotPasswrdUrl = baseUrl+"/Api/User/Ios/en/ForgotPassword"
let changePasswrdUrl = baseUrl+"/Api/User/Ios/en/ChangePasswordOTP"
let resendChngeOtpUrl = baseUrl+"/Api/User/Ios/en/Resend/Mobile/OTP"
let loginUrl = baseUrl+"/Api/v1/User/Ios/en/Login/Process"
let getUserDetailsUrl = baseUrl+"/Api/v1/User/Ios/en/GetUserDetails"
let logOutUrl = baseUrl+"/Api/v1/User/Ios/en/Logout"
let changePasswrd = baseUrl+"/Api/User/Android/en/ChangePassword"
let redeemPromoUrl = baseUrl+"/Api/v1/User/Android/en/requestPromo"

let URL_initiateLoadMoney = baseUrl+"/Api/v1/User/Ios/en/LoadMoney/InitiateRequest"
let URL_LoadMoneyRedirect = baseUrl+"/Api/v1/User/Android/en/LoadMoney/Redirect"
let offersUrl = baseUrl+"/Api/v1/User/Android/en/requestPromo"
let getTransactions = baseUrl+"/Api/v1/User/Android/en/GetTransactions"
let fetchVirtualCard = baseUrl+"/Api/v1/User/Ios/en/FetchVirtualCard"
let getBalance = baseUrl+"/Api/v1/User/Ios/en/GetBalance"
let generateVirtualCard = baseUrl+"/Api/v1/User/Ios/en/GenerateVirtualCard"
let physicalCardRequst = baseUrl+"/Api/v1/User/Ios/en/PhysicalCardRequest"
let addPhysicalCard = baseUrl+"/Api/v1/User/Ios/en/AddPhysicalCard"

let activatePhysicalCard = baseUrl+"/Api/v1/User/Android/en/ActivatePhysicalCard"
let fetchPhysicalCard = baseUrl+"/Api/v1/User/Ios/en/FetchPhysicalCard"
let resendOtpPhysicalCard = baseUrl+"Api/v1/User/Android/en/ResendActivationCode"
    //Customer Support URLs
let URL_QUERYTYPE_LIST = URL_MAIN_CUSTOMER_SUPPORT + "MobileTicket/Component"
let URL_REPORT_QUERY = URL_MAIN_CUSTOMER_SUPPORT + "MobileTicket/Ticket"
let URL_QUERIES = URL_MAIN_CUSTOMER_SUPPORT + "MobileTicket/GetTicket/Email"



/// checking the network reachability functionality
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
    }
}

