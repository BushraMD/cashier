//
//  ReddeemVC.swift
//  Cashier
//
//  Created by MSewa on 03/04/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class ReddeemVC: UIViewController {

    @IBOutlet weak var nav_View: UIView!
    @IBOutlet weak var back_Btn: UIButton!
    @IBOutlet weak var promo_Tf: UITextField!
    @IBOutlet weak var submt_Btn: UIButton!
    @IBOutlet weak var main_View: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnGrdntBackground()
    }
    @IBAction func back_Tap(_ sender: Any) {
        [self.dismiss(animated: true, completion: nil)];
    }
    func btnGrdntBackground(){
        
        let gradieGet:CAGradientLayer = CAGradientLayer()
        
        let colorTo = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        
        let colorBot = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        
        gradieGet.colors = [colorBot, colorTo]
        
        gradieGet.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        gradieGet.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        gradieGet.frame = submt_Btn.bounds
        
        gradieGet.cornerRadius = 10
        
        submt_Btn.layer.insertSublayer(gradieGet, at: 0)
    }
   
    @IBAction func submt_Tap(_ sender: Any) {
        
        
        var jsonString = String()
        let reqDict = ["promoCode":self.promo_Tf.text!] as [String : Any]
        
        do {
            
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: redeemPromoUrl, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00" {
                
                DispatchQueue.main.async {
                    
                    
                    
                }
                
            }
                
            else {
                
                print("Something went wrong!")
                
            }
            
        }
        
        
        
        
        
        
        
    }
}
