//
//  LoadMoneyViewC.swift
//  Cashier
//
//  Created by MSEWA on 13/01/1940 Saka.
//  Copyright © 1940 Saka MSEWA. All rights reserved.
//

import UIKit
import Razorpay

class LoadMoneyViewC: BaseViewC,UITextFieldDelegate, RazorpayPaymentCompletionProtocolWithData
{
    
    @IBOutlet weak var lblLoadMoney: UILabel!
    @IBOutlet weak var txtEnterAmount: UITextField!
    @IBOutlet weak var btnLoadMoney: UIButton!
    @IBOutlet weak var viewTextfield: UIView!
    @IBOutlet weak var viewLoadMoneyPopup: UIView!
    @IBOutlet weak var viewUnderLoadMoneyPopup: UIView!
    
    var razorpay: Razorpay!
    var myString:NSString = "LOAD MONEY TO CARD"
    var myMutableString = NSMutableAttributedString()
    var strErrorMsg = String()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    //MARK: View Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpView()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: IBAction Methods
    @IBAction func tapBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tapLoadMoney(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if validateFields()
        {
            self.viewLoadMoneyPopup.isHidden = false
        }
        else
        {
            let alert = UIAlertController(title: "Cashier", message: strErrorMsg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func tapDismiss(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.viewLoadMoneyPopup.isHidden = true
    }
    
    @IBAction func tapAccept(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.viewLoadMoneyPopup.isHidden = true
        self.apiCallInitiateLoadMoney()
    }
    
    //MARK: UITextfield Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        self.addDoneToolBar(onTextField: textField, target: self, selector: #selector(tapDone))
        return true
    }
    
    //MARK: Private Methods
    func setUpView()
    {
        //Button Gradient Colour
        let gradient:CAGradientLayer = CAGradientLayer()
        let colorTopGoogleBtn = UIColor(red: 29.0/255.0, green: 18.0/255.0, blue: 59.0/255.0, alpha: 1.0).cgColor
        let colorBottomGoogleBtn = UIColor(red: 239.0/255.0, green: 24.0/255.0, blue: 33.0/255.0, alpha: 1.0).cgColor
        gradient.colors = [colorBottomGoogleBtn, colorTopGoogleBtn]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = btnLoadMoney.bounds
        gradient.cornerRadius = 10
        btnLoadMoney.layer.insertSublayer(gradient, at: 0)
        
        //View Border and shadow
        viewTextfield.layer.cornerRadius = 15
        viewTextfield.layer.shadowColor = UIColor.darkGray.cgColor
        viewTextfield.layer.shadowOffset = CGSize.zero
        viewTextfield.layer.shadowOpacity = 2.0
        viewTextfield.layer.shadowRadius = 2.0
        viewTextfield.layer.masksToBounds =  false
        viewTextfield.layer.backgroundColor = UIColor.white.cgColor
        
        //Label text
        myMutableString = NSMutableAttributedString(string: myString as String, attributes: [NSAttributedStringKey.font:UIFont(name: "Helvetica Bold", size: 18.0)!])
        print(myMutableString.length)
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSRange(location:0,length:10))
        myMutableString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.purple, range: NSRange(location:11,length:7))
        lblLoadMoney.attributedText = myMutableString
        
        self.viewLoadMoneyPopup.isHidden = true
        viewUnderLoadMoneyPopup.layer.cornerRadius = 10
    }

    func validateFields() -> Bool
    {
        if (txtEnterAmount.text?.isEmpty)!
        {
            strErrorMsg = "Please enter the amount."
            return false
        }
        
        return true
    }
    
    @objc func tapDone()
    {
        self.view.endEditing(true)
    }
}

extension LoadMoneyViewC
{
    //MARK: API Call Initiate Money
    func apiCallInitiateLoadMoney()
    {
        var jsonString = String()
        let reqDict = ["sessionId": UserModel.shared.userSessionId, "amount": self.txtEnterAmount.text!] as [String : Any]
        print(reqDict)
        
        do {
            let jsonData =  try JSONSerialization.data(withJSONObject: reqDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: URL_initiateLoadMoney, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")

            guard let code = response["code"] as? String else { return }
            if code == "S00"
            {
                print("success")
                self.setUpApiResponse(response)
            }
            else if code == "F00"
            {
                API_Service.alert(kConstAppName, message: (response["message"] as! String), view: self)
            }
            else
            {
                print("Something went wrong!")
                API_Service.alert(applicationName, message: "Oops!Something went wrong", view: self)
            }
        }
    }
    
    func setUpApiResponse(_ response: AnyObject)
    {
        
        let keyID = "rzp_test_zPEcivDLFCp2ls"
        //response["key_id"] as? String
        var amount = Int()
        DispatchQueue.main.async {
            amount = Int(self.txtEnterAmount.text!)! * 100
        }
        
        UserModel.shared.loadMoneyAuthRefNo = response["authReferenceNo"] as? String
        UserModel.shared.loadMoneyTransactionRefNo = response["transactionRefNo"] as? String
        UserModel.shared.loadMoneySecretKey = response["key_secret"] as? String
        DispatchQueue.main.async {
            UserModel.shared.loadMoneyAmount = self.txtEnterAmount.text
        }
        
        
        razorpay = Razorpay.initWithKey(keyID, andDelegateWithData: self)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.showPaymentForm(amount)
        }
    }
    
    //MARK: API Call RazorPay
    func showPaymentForm(_ amount: Int)
    {
        let options: [String:Any] = [
            "amount" : amount,
            "description": UserModel.shared.loadMoneyTransactionRefNo ?? "",
            "image": "https://cards.mycashier.in/IplCards/resources/User/NewUi/img/logo.svg",
            "currency": "INR",
            "name": "Load Money",
            "prefill": [
                "contact": self.appDelegate.mobileNumber,
                "email": self.appDelegate.email
            ]
        ]
        self.razorpay.open(options)
    }
    
    //MARK: RazorPay Delegates
    func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?)
    {
        print("failed")
        let paymentID = ""
        let status = "Failed"
        let virtualCard = "yes"
        
        apiLoadMoneyRedirect(paymentID, status: status, virtualCard: virtualCard)
    }
    
    func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?)
    {
        print(response ?? [:])
        var response = response as? [String:String] ?? [:]
        let paymentID = response["razorpay_payment_id"]
        let status = "Captured"
        let virtualCard = "yes"
        
        apiLoadMoneyRedirect(paymentID!, status: status, virtualCard: virtualCard)
    }
    
    //MARK: API Call Redirect Money
    func apiLoadMoneyRedirect(_ paymentID: String, status: String, virtualCard: String)
    {
        var jsonString = String()
        let amount = Int(UserModel.shared.loadMoneyAmount)! * 100
        
        let requestParams = ["transactionRefNo":UserModel.shared.loadMoneyTransactionRefNo ?? "",
                             "sessionId":UserModel.shared.userSessionId,
                             "status":status,
                             "authReferenceNo":"",
                             "virtualCard":virtualCard,
                             "amount":String(amount),
                             "paymentId":paymentID] as [String : Any]
        
        print(requestParams)
        
        do {
            let jsonData =  try JSONSerialization.data(withJSONObject: requestParams, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
        } catch let myJSONError {
            print(myJSONError)
        }
        
        API_Service.postUrlSession(urlString: URL_LoadMoneyRedirect, params: jsonString) { (response) in
            
            print("response =====>>>> \(response)")
            
            guard let code = response["code"] as? String else { return }
            
            if code == "S00"
            {
                print("success")

                let alert = UIAlertController(title: kConstAppName, message: (response["message"] as! String), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                 self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: kConstAppName, message: (response["message"] as! String), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
}
