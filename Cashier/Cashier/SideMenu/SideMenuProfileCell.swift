//
//  SideMenuProfileCell.swift
//  Cashier
//
//  Created by Apple on 25/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//


import UIKit


class SideMenuProfileCell: UITableViewCell {

    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userCredits: UILabel!
    @IBOutlet weak var userType: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    

}
