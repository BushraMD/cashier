//
//  SideMenuCell.swift
//  Cashier
//
//  Created by AncileMac2 on 24/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryName.minimumScaleFactor = 0.6
        categoryName.adjustsFontSizeToFitWidth = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
