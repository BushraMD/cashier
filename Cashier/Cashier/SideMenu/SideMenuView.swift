//
//  SideMenuView.swift
//  Cashier
//
//  Created by AncileMac2 on 24/03/18.
//  Copyright © 2018 MSEWA. All rights reserved.
//

import UIKit


protocol SendIndexDelegate{
    func sendIndex(_: Int)
}



class SideMenuView: UIView, UITableViewDelegate, UITableViewDataSource {

  
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var rightSideMenuTable: UITableView!
    
    //var webView = UIWebView()
    
    var delegate : SendIndexDelegate?
    var sideMenuCell = SideMenuCell()
    var sideMenuProfileCell = SideMenuProfileCell()
    
    
    var categoryNames = ["Home", "Physical Card", "Share", "Transaction Statement","Redeem Promo Code","Settings","Rate Us","Sign Out", "Customer Support"]
    var categoryNames1 = ["Customer Support"]
    
    
    var categoryImages = ["home", "card", "Share", "TS", "RC", "Settings", "Rate","signOut","customer"]
    
    
    override func draw(_ rect: CGRect) {
        
        rightSideMenuTable.delegate = self
        rightSideMenuTable.dataSource = self
        
        rightSideMenuTable.isScrollEnabled = true
        
    }
    
    
    override func awakeFromNib() {
        
        rightSideMenuTable.estimatedRowHeight = 80
        rightSideMenuTable.rowHeight = UITableViewAutomaticDimension
        
        //rightSideMenuTable.separatorColor = UIColor(colorLiteralRed: 82.0/255, green: 196.0/255, blue: 214.0/255, alpha: 1.0)
        rightSideMenuTable.isScrollEnabled = false
        rightSideMenuTable.separatorStyle = UITableViewCellSeparatorStyle.none
        rightSideMenuTable.register(UINib(nibName: "SideMenuCellXib", bundle: nil), forCellReuseIdentifier: "SideMenuCell")
        
         rightSideMenuTable.register(UINib(nibName: "SideMenuProfileCellxib", bundle: nil), forCellReuseIdentifier: "SideMenuProfileCell")
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 2
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return 1
        
        } else {
            
            return categoryNames.count
        }
//        else {
//
//            return categoryNames1.count
//        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            sideMenuProfileCell = rightSideMenuTable.dequeueReusableCell(withIdentifier: "SideMenuProfileCell", for: indexPath) as! SideMenuProfileCell
            sideMenuProfileCell.userName.text = UserModel.shared.userName
            sideMenuProfileCell.userType.text = UserModel.shared.userType
              sideMenuProfileCell.lastName.text = UserModel.shared.lastName
            
            return sideMenuProfileCell
            
        } else  {
            
            sideMenuCell = rightSideMenuTable.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
            
            sideMenuCell.categoryName.text = categoryNames[indexPath.row]
            
            let catImage = UIImage(named: categoryImages[indexPath.row])
            
           sideMenuCell.categoryImage.image = catImage
            
            return sideMenuCell
            
        }
//            else {
//
//            sideMenuCell = rightSideMenuTable.dequeueReusableCell(withIdentifier: "SideMenuCell", for: indexPath) as! SideMenuCell
//
//            sideMenuCell.categoryName.text = categoryNames1[indexPath.row]
//
//            let catImage = UIImage(named: categoryImages[indexPath.row])
//
//            //sideMenuCell.categoryImage.image = catImage
//
//            return sideMenuCell
//        }
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            
        } else {
        delegate?.sendIndex(indexPath.row)
        }
        
        hidePopUpView()
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            
             return CGFloat(100)
            
        } else {
            
             return CGFloat(UIScreen.main.bounds.size.height/12)
        }
    }
    
    
    func hidePopUpView() {   // To hide the PopUp
        
        let transDown = CATransition()
        transDown.duration = 0.7
        transDown.type = kCATransitionPush
        transDown.subtype = kCATransitionFromRight
        transDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        self.layer.add(transDown, forKey: nil)
        self.isHidden = true
    }

}
